using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrackOut : MonoBehaviour
{
    public Image image;
    [SerializeField, Range(0.01f, 2f)]
    public float brackToWhiteSpeed = 0.05f;
    [SerializeField, Range(0.01f, 2f)]
    public float whiteToBrackSpeed = 0.01f;
    public float alpha = 0;

    public bool actionBrackOut = false;
    // Start is called before the first frame update

    public void BlacktoWhite()
    {
        alpha = 1;
        StartCoroutine(BlacktoWhiteCoroutine());
    }

    public void WhitetoBrack()
    {
        alpha = 0;
        StartCoroutine(WhitetoBlackCoroutine());
    }

    IEnumerator BlacktoWhiteCoroutine()
    {
        while (true)
        {
            if (alpha <= 0)
            {
                break;
            }
            alpha -= brackToWhiteSpeed * Time.deltaTime;
            image.color = new Color(0, 0, 0, alpha);
            yield return null;

            

        }
    }

    /*float time;
    private void Update()
    {
        time += Time.deltaTime;

        if(time > 5 && !tf)
        {
            tf = true;
            if (tf)
            {
                
                WhitetoBrack();
                
            }
        }
    }*/

    IEnumerator WhitetoBlackCoroutine()
    {
        while (true)
        {
            if (alpha >= 1)
            {
                break;
            }
            alpha += whiteToBrackSpeed * Time.deltaTime;
            image.color = new Color(0, 0, 0, alpha);
            yield return null;
        }
    }
}
