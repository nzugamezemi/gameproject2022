using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//　オブジェクトの詳細
public struct GameObjectsDetail
{
    public int number;
    public GameObject enterObject;
}


// 宝のサイズ
public enum TreasureType
{
    SmallTreasure,
    BigTreasure
}

public class TreasureDetail : MonoBehaviour
{

    //[SerializeField] Issa_obtainable obt;//取得可能なクラスのオブジェクト

    public GameObjectsDetail god = new GameObjectsDetail();
    public TreasureType type;
    public int number = 0;

    public jmp.ItemInfo Infomation = new jmp.ItemInfo();

    public bool analyzed = false;
    public bool get = false;
    TreasureDetail()
    {
        god.number = this.number;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Update()
    {
        if (get)
        {
            gameObject.SetActive(false);
        }
        if (analyzed)
        {
            gameObject.SetActive(false);
        }
    }
}
