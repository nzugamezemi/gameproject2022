using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MoveTask :MonoBehaviour, TutorialTask
{

    public Player movetut;
    public string infomation;
    public string title;

    private void Start()
    {

        movetut = GetComponent<Player>();
    }
    public bool CheckTask()
    {
        if (movetut != null)
        {
            float move_x = movetut.movementInput.x;
            float move_y = movetut.movementInput.y;
            if(move_x > 0 || move_y > 0)
            {
                return true;
            }
        }
        return false;
        
    }

    public string Infomation() {
        return infomation;
    }

    public void Tasksetting() {
    }

    public string Title()
    {
        return title;
    }
}
