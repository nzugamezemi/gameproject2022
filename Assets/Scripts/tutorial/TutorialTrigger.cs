using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialTrigger : MonoBehaviour
{
    public string targetTag = "Player";
    public string nameFilter = "";

    public string enterAction = "hello";

    public string exitAction = "goodbye";

    public TutorialManager tutorialManager;

    public bool ExitTutorialArea = false;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == targetTag)
        {
            if (nameFilter != "" && other.gameObject.name.IndexOf(nameFilter) > -1)
            {
                if (tutorialManager != null && enterAction != "")
                {
                    tutorialManager.BroadcastMessage("OnPlayerAction", enterAction);
                    if (ExitTutorialArea)
                    {
                        tutorialManager.ShutDown();
                    }

                }

                
            }
        }
        
    }

    void OnTriggerExit(Collider other)
    {
        if (nameFilter != "" && other.gameObject.name.IndexOf(nameFilter) > -1)
        {
           if (tutorialManager != null && exitAction != "")
            {
                tutorialManager.BroadcastMessage("OnPlayerAction", exitAction);
            }
        }
    }
}
