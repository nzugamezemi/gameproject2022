using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System;
public class TutorialManager : MonoBehaviour, TutorialStep.Delegate
{
    public UITrackObject TutorialUI;
    public TextMeshProUGUI TutorialText;
    public GameObject TutorialCanvas;

    public string firsttutorialtext;

    public Image GuideUI;

    protected TutorialTask currentTask;//���݂̃^�X�N
    // public List<TutorialTask> tutorialTasks;

    int currentTutorialStep = 0;
    public TutorialStep[] tutorialSteps;

    public GameObject[] Moveguides;
    public int guidecount = 0;

    bool taskExecuted;//���s�t���O

    Player player;
    IEnumerator waitCoroutine = null;


    // Start is called before the first frame update
    void Start()
    {
        // GameObject Diver = GameObject.Find("Diver(Clone)");
        // tutorialTasks = new List<TutorialTask>
        // { 
        //     Diver.gameObject.GetComponent<MoveTask>(),
        // };

        TutorialUI.trackedObject = GameObject.Find("Diver(Clone)");

        TutorialText.text = firsttutorialtext;

        player = GameObject.Find("Robot(Clone)").GetComponent<Player>();
        if (player.destroyAndLoad)
        {
            TutorialCanvas.SetActive(false);
        }
        foreach (TutorialStep step in tutorialSteps)
        {
            step.actionDelegate = this;
        }

        for (int i = 0; i < tutorialSteps.Length - 1; i++)
        {
            tutorialSteps[i].nextStep = tutorialSteps[i + 1];
        }
    }

    //private IEnumerator SetcurrentTask(TutorialTask task)
    //{
    //    task.Tasksetting();
    //    currentTask = task;
    //    taskExecuted = false;

    //    TutorialText.text = task.Infomation();

    //    yield return 1f;

    //}

    // Update is called once per frame
    void Update()
    {
        // if(currentTask != null && !taskExecuted)
        // {
        //     if (currentTask.CheckTask())
        //     {
        //         taskExecuted = true;
        //         tutorialTasks.RemoveAt(0);
        //         var nextTask = tutorialTasks.FirstOrDefault();
        //         if (nextTask != null)
        //         {
        //             StartCoroutine(SetcurrentTask(nextTask));
        //         }
        //     }
        // }
    }

    void OnPlayerAction(string action)
    {
        if (currentTutorialStep < tutorialSteps.Length)
        {
            tutorialSteps[currentTutorialStep].OnAction(action);
        }
    }

    public void TutorialStepStart(TutorialStep step, float delay)
    {
        waitCoroutine = ShowTutorialStep(step, delay);
        StartCoroutine(waitCoroutine);
    }

    public void TutorialStepEnd(TutorialStep step, float delay)
    {
        waitCoroutine = EndTutorialStep(step, delay);
        StartCoroutine(waitCoroutine);
    }

    public void GotoNextStepNow(TutorialStep step)
    {
        if(waitCoroutine != null)
        {
            StopCoroutine(waitCoroutine);
            waitCoroutine = null;

            GotoNextStep(step);
        }
    }

    void ShowTutorial(TutorialStep step)
    {
        TutorialText.text = step.Description;

        if (step.Moveguide)
        {

            Animator guide_ani = Moveguides[guidecount].GetComponentInChildren<Animator>();
            ShowGuide(guide_ani);
        }
        if (step.TutorialUI != null)
        {           

            GuideUI.sprite = step.TutorialUI;

            GuideUI.color = new Color(255, 255, 255, 225);
        }

        if (step.Moveguide)
        {

            Animator guide_ani = Moveguides[guidecount].GetComponentInChildren<Animator>();
            ShowGuide(guide_ani);
        }


        // ここでチュートリアル情報を表示する
        Debug.Log(step.Description);
    }

    void GotoNextStep(TutorialStep step)
    {
        // 次のステップへ
        currentTutorialStep += 1;

        // 自動再生するステップの処理
        if (currentTutorialStep < tutorialSteps.Length)
        {
            if (tutorialSteps[currentTutorialStep].AutoStart)
            {
                tutorialSteps[currentTutorialStep].StartStep();
            }
        }
        if(currentTutorialStep == tutorialSteps.Length)
        {
            TutorialCanvas.SetActive(false);
        }
    }

    IEnumerator ShowTutorialStep(TutorialStep step, float delay)
    {
        yield return new WaitForSeconds(delay);

        waitCoroutine = null;

        ShowTutorial(step);
    }

    IEnumerator EndTutorialStep(TutorialStep step, float delay)
    {

        if (step.Moveguide)
        {
            Animator guide_ani = Moveguides[guidecount].GetComponentInChildren<Animator>();
            HideGuide(guide_ani);
        }
        yield return new WaitForSeconds(delay);

        waitCoroutine = null;

        

        // ここでチュートリアル情報を消す
        Debug.Log("End " + step.Description);

        GotoNextStep(step);
    }


    void ShowGuide(Animator ani)
    {
        ani.SetBool("isShown", true);
    }

    void HideGuide(Animator ani)
    {
        ani.SetBool("isShown", false);
        guidecount += 1;
    }

    public void ShutDown()
    {
        if(currentTutorialStep < tutorialSteps.Length)
        {
            TutorialCanvas.SetActive(false);
        }
    }
}
