using UnityEngine;
using System;
using UnityEngine.UI;

[Serializable]
public class TutorialStep
{
    

    public string Description = "Title";

    public bool AutoStart = false;
    
    public string StartAction = "PlayerSpawn";

    public float StartDelay = 0;

    public string EndAction = "PlayerMove";

    public float EndDelay = 0;

    public Sprite TutorialUI = null;

    public bool Moveguide = false;

     [HideInInspector]
    public Delegate actionDelegate = null;

    [HideInInspector]
    public TutorialStep nextStep = null;

    bool active = false;


    public interface Delegate {
        public void TutorialStepStart(TutorialStep step, float delay);
        public void TutorialStepEnd(TutorialStep step, float delay);
        public void GotoNextStepNow(TutorialStep step);
    }

    public void StartStep()
    {
        active = true;
        actionDelegate?.TutorialStepStart(this, StartDelay);
    }

    public void EndStep()
    {
        active = false;
        actionDelegate?.TutorialStepEnd(this, EndDelay);
    }

    public void OnAction(string action)
    {
        Debug.Log("OnAction: " + action + ", active: " + active + ", endaction: " + EndAction);
        if (action == StartAction && !active)
        {
            StartStep();
        }

        if (action == EndAction && active)
        {
            EndStep();
        }

        if (!active && nextStep != null && !nextStep.AutoStart && nextStep.StartAction == action)
        {
            actionDelegate?.GotoNextStepNow(this);
        }

    }
}