using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface TutorialTask
{
    string Title();
    string Infomation();

    void Tasksetting();

    bool CheckTask();
}