using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tobira : MonoBehaviour
{

    public GameObject iwa;
    public Vector3 iwaY = Vector3.zero;
    public Vector3 himoY = Vector3.zero;
    public Vector3 limit = Vector3.zero;
    public Vector3 zero = Vector3.zero;
    Rigidbody rb;

    public float iwaTy;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //iwaTy = iwa.transform.position.y;
        iwaY = iwa.transform.position;
        himoY = transform.position;
        limit = new Vector3(0f, 2f, 0f);
        zero = himoY;
    }
    
    void Open() 
    {
        //Debug.Log(transform.position.magnitude);
        if (transform.position.magnitude > himoY.magnitude && transform.position.magnitude < himoY.magnitude + 8f)
        {
            iwa.transform.position = iwaY + (himoY - transform.position);
        }
        else
        {
            rb.velocity = Vector3.zero;
            if(transform.position.magnitude > himoY.magnitude + 8f)
            {
                rb.velocity = Vector3.Lerp(rb.velocity, transform.up * 10f, 0.1f);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        Open();
    }
}
