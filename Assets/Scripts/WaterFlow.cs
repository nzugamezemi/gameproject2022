using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterFlow : MonoBehaviour
{
    public Vector3 force;
    public float zforce = 100;

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            Rigidbody rb = other.attachedRigidbody;
            if (rb)
            {
                rb.AddForce(transform.forward * zforce * -1 + force, ForceMode.Force);
            }
        }
        
    }
}
