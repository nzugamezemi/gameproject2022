using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class HoldItem : MonoBehaviour
{
    public Rigidbody pickupTarget;
    Joint joint;
    Animator animator;

    Rigidbody diverRb;

   // public bool isBring = false;

    // Start is called before the first frame update
    void Start()
    {
        //animator = GetComponentInChildren<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Pickup"))
        {
            Debug.Log("Pickup: " + other.name);
            pickupTarget = other.attachedRigidbody;

            /*diverRb = GetComponentInParent<Rigidbody>();

            diverRb.constraints = RigidbodyConstraints.FreezePosition;
            diverRb.constraints = RigidbodyConstraints.FreezeRotation;*/
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.attachedRigidbody == pickupTarget)
        {
            pickupTarget = null;

            //diverRb.constraints = RigidbodyConstraints.None;
        }
    }

    void OnGrab(bool isGrabbing)
    {
        Debug.Log("Grab: " + isGrabbing);
        //isBring = isGrabbing;

        if (!isGrabbing && joint != null)
        {
            //animator.SetBool("Bring", isBring);
            Destroy(joint);
            joint = null;
            if (pickupTarget != null)
            {
                Vector3 p = pickupTarget.position;
                p.z = transform.parent.position.z;
                pickupTarget.position = p;
                pickupTarget = null;
            }
        }

        if (isGrabbing && pickupTarget != null)
        {
            
           // animator.SetBool("Bring",isBring);

            joint = transform.parent.gameObject.AddComponent<FixedJoint>();
            joint.connectedBody = pickupTarget;
            Vector3 d = (pickupTarget.position - joint.transform.position) * 2.5f;
            //Vector3 d = pickupTarget.position - joint.transform.position;
            joint.connectedAnchor = d;
        }
    }

   void OnGrab(InputValue inputValue)
    {
        OnGrab(inputValue.isPressed);
    }

    void FixedUpdate()
    {
       //if(pickupTarget)
        
    }

}
