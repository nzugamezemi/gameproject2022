using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using Cinemachine;

public class PlayersManager : MonoBehaviour
{
    public Sonar robotSonar;
    public GameObject robotPrefab, diverPrefab;
    public GameObject inventory;

    public GameObject[] playerActionListeners;

    GameObject diver, robot;
    PlayerInput robotInput;
    PlayerInput diverInput;
    public bool destroyPlayer = false;
    public bool pauseUI = false;
    public bool destroyAndLoad = false;
    Transform diverRespawnPosition;
    Transform robotRespawnPosition;

    Playerstatus diverStatus, robotStatus;
    Depth depth;
    BrackOut brackOut;
    public TreasureJudge treasureJudge;

    public List<GameObject> nullGameObjects = new List<GameObject>();

    public GameObject UI;
    public GameObject robotUIObject;
    public GameObject heatEffect;
    public GameObject healEffect;
    GameObject brackOutPanel;
    GameObject spotLight;
    public Vector3[] startTreasurePositions;

    public UITrackObject robotUI;
    Robot_UI robot_UI;
    public SoundManager soundManager;

    public SaveManager saveManager;

    public float startOxy = 1000;
    public float startBat = 1000;
    public float damageSpeed = 10;
    public float UIAlphaSpeed = 1f;
    [SerializeField, Range(0, 100)]
    float healValueRatio = 10;
    public float healValue = 0;
    public float batteryValue = 100;
    public int ReSpawnTime = 3;

    public int healNum = 1;

    public List<GameObject> gimmics = new List<GameObject>();

    public GameObject[] treasures;

    private void Awake()
    {

        ObjectsFind();
        SpawnPosition();
        SpawnPlayers();
    }

    

    public GameObject Diver
    {
        get { return diver; }
        set { diver = value; }
    }

    public GameObject Robot
    {
        get { return robot; }
        set { robot = value; }
    }

    private void Start()
    {

        healValue = startOxy * (healValueRatio * 0.01f);
    }

    void ObjectsFind()
    {
        GameObject[] gimmicObjects = GameObject.FindGameObjectsWithTag("Gimmick");


        foreach (var i in gimmicObjects)
        {
            gimmics.Add(i);
        }

        treasures = GameObject.FindGameObjectsWithTag("Treasure");
        startTreasurePositions = new Vector3[treasures.Length];


        for (var i = 0; i < treasures.Length; i++)
        {
            treasures[i] = treasures[i];
            startTreasurePositions[i] = new Vector3(treasures[i].transform.position.x, treasures[i].transform.position.y, treasures[i].transform.position.z);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if(diverStatus.Oxygen <= 0 && !brackOut.actionBrackOut)
        {
            brackOut.actionBrackOut = true;
            Respawn();
        }

        if (robotInput && robotStatus)
        {
            if (robotStatus.Battery > 0 && !robotInput.inputIsActive)
            {
                robotInput.ActivateInput();
                robot.tag = "Untagged";
                if(!spotLight.activeSelf)
                {
                    spotLight.SetActive(true);
                }
            }
            else if (robotStatus.Battery <= 0 && robotInput.inputIsActive)
            {
                robotInput.DeactivateInput();
                robot.tag = "Pickup";
                if (robotSonar)
                {
                    robotSonar.visible = false;
                }

                if (spotLight.activeSelf)
                {
                    spotLight.SetActive(false);
                }
            }
        }

    }

    void SpawnPosition()
    {
        diverRespawnPosition = transform.GetChild(1).GetChild(0);
        robotRespawnPosition = transform.GetChild(1).GetChild(1);
    }

    void SpawnPlayers()
    {
        var p2 = PlayerInput.Instantiate(diverPrefab, controlScheme: "Gamepad", pairWithDevice: Gamepad.current);
        var p1 = PlayerInput.Instantiate(robotPrefab, controlScheme: "Gamepad", pairWithDevice: Gamepad.current);
        
        // チュートリアル
        PlayerActionSender p1Sender = p1.GetComponentInChildren<PlayerActionSender>();
        PlayerActionSender p2Sender = p2.GetComponentInChildren<PlayerActionSender>();

        if (p1Sender != null) {
            p1Sender.listeners = playerActionListeners;
        }

        if (p2Sender != null) {
            p2Sender.listeners = playerActionListeners;
        }

        saveManager = GetComponent<SaveManager>();

        diver = p2.gameObject;
        robot = p1.gameObject;

        diver.transform.position = diverRespawnPosition.position;
        robot.transform.position = robotRespawnPosition.position;

        if (robotSonar)
        {
            robotSonar.target = robot.transform;
            Player robotPlayer = robot.GetComponent<Player>();
            if (robotPlayer)
            {
                robotPlayer.onSearch += (pressed) =>
                {
                    robotSonar.ResetTime();
                    robotSonar.visible = pressed;
                };
            }

            robotSonar.visible = false;
        }

        treasureJudge = diver.GetComponentInChildren<TreasureJudge>();

        diverStatus = diver.GetComponent<Playerstatus>();
        robotStatus = robot.GetComponent<Playerstatus>();

        diverStatus.Oxygen = startOxy;
        robotStatus.Battery = startBat;

        robot_UI = robot.GetComponent<Robot_UI>();

        soundManager = GetComponent<SoundManager>();

        if(UI != null)
        {
            depth = UI.transform.GetChild(0).GetComponent<Depth>();

            brackOutPanel = UI.transform.root.GetChild(4).GetChild(0).gameObject;
            brackOut = brackOutPanel.GetComponent<BrackOut>();
            brackOut.image = brackOutPanel.GetComponent<Image>();

            if (depth != null)
            {
                depth.target = diver;
            }
        }

        saveManager.players(diver, robot);

        robotUIObject = GameObject.Find("RobotUI");

        if (robotUI != null)
        {
            robotUI.trackedObject = robot;
        }
        robotInput = robot.GetComponent<PlayerInput>();
        diverInput = diver.GetComponent<PlayerInput>();

        spotLight = Robot.transform.GetChild(6).gameObject;

        brackOut.BlacktoWhite();

        CinemachineVirtualCamera vc = GetComponentInChildren<CinemachineVirtualCamera>();
        if (vc)
        {
            vc.Follow = p2.transform;
        }

        saveManager.playersManager = GetComponent<PlayersManager>();

        if (inventory)
        {
            saveManager.inventory = inventory.GetComponent<jmp.Inventory>();
        }

        saveManager.filePath = Application.dataPath + "/savedate.json";

        if (!destroyPlayer)
        {

            if (StartSelect.saveDate)
            {
                
                Debug.Log(saveManager.filePath);


                destroyAndLoad = true;

                saveManager.Load();
                
            }
        }
        else
        {
            destroyAndLoad = destroyPlayer;
        }
        
        Debug.Log(diverStatus.Oxygen);

        robot_UI.Maxoxygen = diverStatus.Oxygen;

        diverStatus.damageAreaEnter = false;

        robotInput.DeactivateInput();
        diverInput.DeactivateInput();

        Input();

        
        brackOut.actionBrackOut = false;
    }

    void Input()
    {
        diverInput.ActivateInput();
        robotInput.ActivateInput();
    }

    void Respawn()
    {
        brackOut.WhitetoBrack();
        RespawnTreasure();

        robotInput.DeactivateInput();
        diverInput.DeactivateInput();

        Invoke("RespwanPlayer", ReSpawnTime);
    }

    void RespawnTreasure()
    {

        for (var i = 0; i < treasures.Length; i++)
        {
            treasures[i].transform.position = new Vector3(startTreasurePositions[i].x, startTreasurePositions[i].y, startTreasurePositions[i].z);
        }
    }

    void RespwanPlayer()
    {
        destroyPlayer = true;
        Destroy(diver);
        Destroy(robot);

        depth.target = null;

        if (nullGameObjects.Count != 0)
        {
            foreach (var nu in nullGameObjects)
            {
                nu.SetActive(true);
            }
            nullGameObjects.Clear();
        }


        SpawnPlayers();
    }
}
