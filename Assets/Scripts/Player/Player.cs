using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System;

public class Player : MonoBehaviour
{
    public float friction = 2f;

    public float runSpeed = 3f; //���鑬�x
    public float acceleration = 10f; //�����x
    public float jumppForce = 10;
    public float turnSmoothing = 0.75f;
    public float turnDeadZone = 0.05f;
    public float idleReturnSmoothing = 0.99f; // ���삵�Ȃ����ɁA�f�t�H���g�̎p���ɖ߂邽�߂̃X���[�W���O
    public Vector3 idleRotation = Vector3.zero;

    public float maxSpeedUp = 2;
    public float maxSpeedDown = -0.5f;
    public float maxSpeedSide = 2;
  
    public float get = 0;

    public bool robotEnter = false;

    [SerializeField]GameObject TestUI;
    [SerializeField] GameObject robotUI;

    public Action<bool> onSearch;

    [SerializeField]
    public float accelerationSpeed = 150f;
    public float decelerationSpeed = 1f;
    public float minBoostSpeed = 2f;

    [SerializeField]int searchBattery = 10;
    [SerializeField] int eleBattery = 10;
    [SerializeField] ushort OxyHealValue = 1000;
    public byte OxygenHealNum = 1;

    public Camera playerCamera = null; //�v���C���[�̃J����

    bool UIControl = false;
    public bool isSwimming = false;

    private GameBehavior gameManager;
    GameObject searchObject;
    //SphereCollider sc;
    GameObject sc;
    BoxCollider bc;

    public PlayerSearch playerSearch;
    Playerstatus playerstatus;
    PlayersManager playersManager;
    public SoundManager soundManager;
    SaveManager saveManager;

    Rigidbody rb;
    Animator animator;
    public GameObject eleParticle;
    //public HoldItem holdItem;
    public Playerstatus ps;
    float battery;

    public TreasureJudge tj;
    public bool isBring;
    public GameObject enterObject;
    public GameObject robot;

    public Vector2 movementInput = Vector2.zero;
    Vector3 groundNormal = Vector3.up; // �n�ʂ̖@���i�m�[�}���j
    Vector3 groundContactPoint = Vector3.zero; // �n�ʂƐڐG���Ă���_�̍��W

    AudioSource audioSource;

    public List<GameObject> treasureList = new List<GameObject>();
    bool canAccele = true;
    public bool isAccele = false; // �ːi���Ă��邩�ǂ���
    public bool electricity = false; //�d�C���o���Ă��邩�ǂ���
    public bool destroyAndLoad = false;

    public bool tutorialMode = true;

    private void OnEnable()
    {
        if (gameObject.name == "Robot(Clone)")
        {
            playersManager = GameObject.Find("Players").GetComponent<PlayersManager>();
            searchObject = transform.GetChild(6).gameObject;
            playerSearch = searchObject.GetComponent<PlayerSearch>();
            searchObject.SetActive(false);
            sc = transform.Find("Electricity").gameObject;
            bc = GetComponent<BoxCollider>();

            

        }
    }
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        animator = GetComponentInChildren<Animator>();
        ps = GameObject.Find("Robot(Clone)").GetComponent<Playerstatus>();

        if (gameObject.name == "Diver(Clone)")
        {
            robot = GameObject.Find("Robot(Clone)");
        }

        if (eleParticle)
        {
            eleParticle.SetActive(false);
        }        

        playerstatus = GetComponent<Playerstatus>();

        if (playerstatus.robot)
        {
            playerstatus.playerSearchObj = searchObject;
        }
        
        if (playersManager != null)
        {
            TestUI = playersManager.UI;
            robotUI = playersManager.robotUIObject;

            if (TestUI != null)
            {
                TestUI.SetActive(true);
            }

            soundManager = playersManager.soundManager;
            destroyAndLoad = playersManager.destroyAndLoad;

            if (!destroyAndLoad)
            {
                BroadcastMessage("OnPlayerAction", "PlayerSpawn");
            }
        }

        audioSource = GetComponent<AudioSource>();
        
        saveManager = GameObject.Find("Players").GetComponent<SaveManager>();

        


    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Analysis"))
        {
            Debug.Log(other.gameObject);
        }

        if (this.name == "Robot(Clone)")
        {
            if (other.CompareTag("WaterFlow"))
            {
                canAccele = true;
            }
        }
    }

    public void Accele(bool pressed)
    {

        if (pressed && !isAccele)
        {
            Debug.Log("Accele");
            isAccele = true;
            // rb.AddForce(transform.right * accelerationSpeed, ForceMode.VelocityChange);
            rb.velocity = transform.right * accelerationSpeed;
        }
        
    }

    public void OnAccele(InputValue input)
    {
         if(canAccele == true)
         {
            Accele(input.isPressed);
            BroadcastMessage("OnPlayerAction", "RobotAccele");

         }


    }


    public void OnUITransparent(InputValue input)
    {
        UITransparent(input.isPressed);
    }

    public void UITransparent(bool input)
    {
        if (input)
        {
            if (!UIControl)
            {
                UIControl = true;
                TestUI.SetActive(false);
                robotUI.SetActive(false);
            }
            else
            {
                UIControl = false;
                TestUI.SetActive(true);
                robotUI.SetActive(true);
            }
        }
    }

    public void Move(Vector2 input)
    {
        if (!isAccele) 
        {
            rb.drag = 0;
            movementInput = input;
        }

        if (input.magnitude > 0.5f)
        {
            rb.drag = 0;

            if (this.name == "Diver(Clone)")
            {
                BroadcastMessage("OnPlayerAction", "DiverMove");
            }
            else 
            {
                BroadcastMessage("OnPlayerAction", "RobotMove");
            }
        }
        else
        {
            if (this.name == "Diver(Clone)")
            {
                rb.drag = 4f;
            }

            if (this.name == "Robot(Clone)")
            {
                rb.drag = 0.2f;
            }

        }
    }
     
    public void OnMove(InputValue inputValue)
    {
        if(enterObject != robot || !isBring || battery <= 0)
        {
            Move(inputValue.Get<Vector2>());
            if (this.name == "Diver(Clone)")
            {
                isSwimming = true;
            }
        }
    }

    public void OnMove1(InputValue inputValue)
    {
        if (enterObject == robot && isBring && battery > 0)
        {
            Move(inputValue.Get<Vector2>());
            if (this.name == "Diver(Clone)")
            {
                isSwimming = true;
            }
        }
    }

    public void OnSearch(InputValue input)
    {
        Search(input.isPressed);
        //onSearch?.Invoke(input.isPressed);
        if (this.name == "Robot(Clone)")
        {
            BroadcastMessage("OnPlayerAction", "Search");
        }

    }

    void Search(bool pressed)
    {
        if (pressed)
        {
            
            if (soundManager != null)
            {
                playerSearch.soundManager = soundManager;
                playerSearch.audioSource = audioSource;
            }

            playerSearch.SearchTex(pressed);
            if (playerstatus.Battery > searchBattery)
            {


                searchObject.SetActive(pressed);
                if (soundManager != null)
                {
                    playerSearch.soundManager = soundManager;
                    playerSearch.audioSource = audioSource;
                }

                playerSearch.SearchTex(pressed);
            }
            else
            {
                playerSearch.SearchTex(pressed);
            }
            
        }
        else
        {
            playerSearch.SearchTex(pressed);
            searchObject.SetActive(pressed);
            
            foreach(var a in playerSearch.treasureSearchObj)
            {
                Destroy(a);
            }
            
            playerSearch.treasureSearchObj.Clear();
        }
    }

    void OnAction(InputValue input)
    {
        Action(input.isPressed);
    }

    public GameObject healObject;

    void Action(bool pressed)
    {
        if(pressed && robotEnter)
        {
            if(playerstatus.Oxygen > 0 && OxygenHealNum > 0)
            {
                playerstatus.Oxygen += OxyHealValue;
                Instantiate(healObject, transform.position, new Quaternion(0,0,0,0));

                OxygenHealNum--;
            }
        }
    }

    void OnElectricity(InputValue inputValue)
    {
        Electricity(inputValue.isPressed);
    }

    public void Electricity(bool isPressed)
    {
        if(this.name == "Robot(Clone)")
        {
            //this.tag = "Robot";
            
            if (playerstatus.Battery > 0)
            {
                playerstatus.Battery -= eleBattery;
                electricity = true;
                eleParticle.SetActive(true);
                // sc.enabled = true;
                sc.SetActive(true);
            }
            BroadcastMessage("OnPlayerAction", "electric");
            // Debug.Log("�d��");
        }
        if(!isPressed || playerstatus.Battery <= 0)
        {
            //this.tag = "Player";
            electricity = false;
            eleParticle.SetActive(false);
           // sc.enabled = false;
            sc.SetActive(false);
            // Debug.Log("�d������");
        }
        
    }

    public Vector3 md;
    public void ApplyMotion()
    {
        // ���͂̓K�p
        Vector2 movementDirection = Vector2.zero;

        movementDirection = movementInput;

        md = movementDirection;

        // �v���[���[�̌�����ς���
        if(isAccele) {

        }
        else if (movementInput.magnitude > 0f)
        {
            if (rb.velocity.magnitude > 0.1f) {
                
                float frontDirectionX = Mathf.Sign(rb.velocity.x);
                Vector3 forward = new Vector3(0, 0, frontDirectionX);

                // ��]�̃f�b�h�]�[��
                float vdot = Vector3.Dot(Vector3.down, rb.velocity.normalized);
                if (vdot > 0f) {
                    if (vdot > 1f - turnDeadZone) {
                        forward = new Vector3(0, 0, Mathf.Sign(transform.forward.z));
                    }
                }
                else {
                    if (vdot < -1f + turnDeadZone) {
                        forward = new Vector3(0, 0, Mathf.Sign(transform.forward.z));
                    }
                }
                
                // Vector3 up = new Vector3(rb.velocity.y * -forward.z, rb.velocity.x * forward.z, 0).normalized;
                Vector3 up = new Vector3(movementInput.y * -forward.z, movementInput.x * forward.z, 0).normalized;
                rb.MoveRotation(Quaternion.Lerp(Quaternion.LookRotation(forward, up), transform.rotation, turnSmoothing));
            }
            
        }
        else
        {
            Vector3 forward = new Vector3(0, 0, Mathf.Sign(transform.forward.z));
            Vector3 up = new Vector3(-Mathf.Sign(forward.z), 0, 0);
            rb.MoveRotation(Quaternion.Lerp(Quaternion.LookRotation(forward, up) * Quaternion.Euler(idleRotation), transform.rotation, idleReturnSmoothing));
        }
        

        if (isAccele) {
            rb.AddForce(rb.velocity.normalized * -decelerationSpeed, ForceMode.Acceleration);

            if(rb.velocity.magnitude < minBoostSpeed) {
                isAccele = false;
            }
        }
        else {
            if (movementInput.magnitude > 0f)
            {
                rb.velocity = Vector3.Lerp(rb.velocity, transform.right * 10f, 0.1f);
                // rb.AddForce(movementDirection * acceleration, ForceMode.Acceleration);
            }
            else 
            {
                if (rb.velocity.magnitude > 0.05f) {
                    rb.AddForce(rb.velocity.normalized * -friction, ForceMode.Force);
                }
                else {
                    rb.velocity = Vector3.zero;
                }
            }
        
            // ���x����
            Vector3 vel = rb.velocity;
            if (Mathf.Abs(vel.x) > maxSpeedSide) {
                vel.x = maxSpeedSide * Mathf.Sign(vel.x);
            }
            if (vel.y < maxSpeedDown) {
                vel.y = maxSpeedDown;
            }
            if (vel.y > maxSpeedUp) {
                vel.y = maxSpeedUp;
            }
            rb.velocity = vel;
        }
        

        // �v���[���[���͂ƒn�ʂ̉E�ƑO��g�ݍ��킹��
        // Vector3 movement = Vector3.right * movementInput.x + Vector3.up * movementInput.y;

        

        if (this.name == "Diver(Clone)" && movementInput == Vector2.zero)
        {
            isSwimming = false;

        }

    }

    Vector3 ProjectOnPlane(Vector3 vector, Vector3 normal)
    {
        return vector;
    }

    void FindRobotElement()
    {
        ps = GameObject.Find("Robot(Clone)").GetComponent<Playerstatus>();

        if (gameObject.name == "Diver(Clone)")
        {
            robot = GameObject.Find("Robot(Clone)");
        }
    }

    void FixedUpdate()
    {
        ApplyMotion();

        if(ps != null)
        {
            battery = ps.Battery;
            if(this.name == "Robot(Clone)")
            {
                if (battery <= 0)
                {
                    rb.drag = 0f;
                    this.tag = "Pickup";
                }
                else
                {
                    this.tag = "Player";
                }
            }
        }

        if(tj != null)
        {
            isBring = tj.isBring;
            enterObject = tj.enterObject;
        }

        if(ps == null || robot == null)
        {
            FindRobotElement();
        }

        if (animator != null)
        {
            if (playerstatus.Oxygen <= 0)
            {
                animator.SetTrigger("Dead");
            }

            if (isBring)
            {
                animator.SetBool("Bring", isBring);
            }
            else
            {
                animator.SetBool("Bring", isBring);
            }

            if (movementInput.magnitude > 0.5f)
            {
                if (rb.velocity.magnitude < 0.1f || Vector2.Dot(movementInput, (Vector2)(rb.velocity)) < 0)
                {
                    animator.SetTrigger("Reset");
                }
                else
                {
                    animator.SetBool("Swimming", isSwimming);
                }
            }
            
        }

    }

    private void Update()
    {
        if (searchObject)
        {
            if(playerstatus.Battery <= searchBattery)
            {
                playerSearch.SearchTex(false);
                searchObject.SetActive(false);
            }
        }
    }

    ContactPoint[] contacts = new ContactPoint[16];

    void OnCollisionStay(Collision collisionInfo)
    {
        // �����ɕǂɐH�����܂Ȃ��悤�ɂ���
        if (collisionInfo.rigidbody == null || Mathf.Approximately(collisionInfo.rigidbody.velocity.magnitude, 0))
        {
            rb.AddForce(collisionInfo.impulse * 0.01f, ForceMode.VelocityChange);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (this.name == "Robot(Clone)")
        {
            if (other.gameObject.tag == "WaterFlow")
            {
                canAccele = false;
            }
        }
    }

}
