using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Playerstatus : MonoBehaviour
{
    public float Oxygen; //�_�f
    public float Battery;//�o�b�e���[

    public float OxygenDecreaseRate = 30f;
    public float BatteryDecreaseRate = 30f;

    int healNum;

    public bool diver;
    public bool robot;

    public GameObject oxygenUI;//�_�f�̃X���C�_�[
    public GameObject oxygenBackUI;
    GameObject batteryUI;//�o�b�e���[�̃X���C�_�[

    GameObject baseArea;
    GameObject heatEffect;
    public GameObject playerSearchObj;

    public bool baseAreaEnter = false, damageAreaEnter = false;

    public Slider oxygenSlider;//�_�f�̃X���C�_�[
    public Slider oxygenBackSlider;
    public Slider batterySlider;//�o�b�e���[�̃X���C�_�[

    PlayersManager playersManager;
    public PlayerSearch playerSearch;
    DamageAreaControl damageAreaControl;

    SoundManager soundManager;
    AudioSource audioSource;
    AudioSource playerAudio;

    

    public Robot_UI robUI;

    public Image oxygenFill;

    CanvasGroup mainCanvasGroup;
    CanvasGroup robotCanvasGroup;

    public Color startColor;

    bool enemyEnter = false;

    // Start is called before the first frame update

    private void OnEnable()
    {
        playersManager = GameObject.Find("Players").GetComponent<PlayersManager>();
        baseArea = GameObject.Find("BaseArea");
        heatEffect = playersManager.heatEffect;
        robUI = GetComponent<Robot_UI>();
        
    }

    void Start()
    {
        baseAreaEnter = false;

        soundManager = playersManager.GetComponent<SoundManager>();

        if(gameObject == playersManager.Diver && playersManager.UI != null)
        {
            oxygenUI = playersManager.UI.transform.GetChild(2).gameObject;
            if (oxygenUI)
            {
                oxygenSlider = oxygenUI.GetComponent<Slider>();
                oxygenSlider.maxValue = playersManager.startOxy;
                oxygenSlider.value = Oxygen;

                oxygenFill = oxygenUI.transform.GetChild(1).GetComponentInChildren<Image>();
                startColor = new Color(255,255,255);
            }
            diver = true;

            oxygenBackUI = playersManager.UI.transform.GetChild(1).gameObject;
            if (oxygenBackUI)
            {
                oxygenBackSlider = oxygenBackUI.GetComponent<Slider>();
                oxygenBackSlider.maxValue = playersManager.startOxy;
                oxygenBackSlider.value = Oxygen;
            }

            if (playersManager.UI)
            {
                mainCanvasGroup = playersManager.UI.GetComponent<CanvasGroup>();
            }

            playerAudio = GetComponent<AudioSource>();

            oxygenBackUI.SetActive(false);
        }

        if(gameObject == playersManager.Robot && playersManager.UI != null)
        {
            if (playersManager.robotUIObject)
            {
                robotCanvasGroup = playersManager.robotUIObject.GetComponent<CanvasGroup>();
                batteryUI = playersManager.robotUIObject.transform.GetChild(0).GetChild(0).gameObject;
            }

            if (batteryUI)
            {
                batterySlider = batteryUI.GetComponent<Slider>();
                batterySlider.maxValue = playersManager.startBat;
                batterySlider.value = Battery;
            }
            robot = true;
        }

        if (heatEffect)
        {
            heatEffect.SetActive(false);
        }

        damageAreaEnter = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if (diver)
            {
                oxygenBackUI.SetActive(true);
                oxygenBackSlider.value = oxygenSlider.value;
                Oxygen -= 1000;
                oxygenSlider.value = Oxygen;
                enemyEnter = true;

                soundManager.playerDamage(playerAudio);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
        /*if (other.gameObject.CompareTag("Enemy"))
        {
            if (diver)
            {
                oxygenBackUI.SetActive(true);
                oxygenBackSlider.value = oxygenSlider.value;

                Oxygen -= 1000;
                oxygenSlider.value = Oxygen;

                
                enemyEnter = true;
            }
        }*/

        if (diver || robot)
        {
            if (other.gameObject == baseArea)
            {
                baseAreaEnter = true;
                //playersManager.robotUIObject.SetActive(false);
                //playersManager.UI.SetActive(false);
            }
        }
        

        if (other.gameObject.CompareTag("DamageArea"))
        {
            damageAreaEnter = true;
            damageAreaControl = other.GetComponent<DamageAreaControl>();

            audioSource = other.GetComponent<AudioSource>();

            
            

            heatEffect.SetActive(true);
        }

        if (other.gameObject.CompareTag("HealArea"))
        {
            if (diver)
            {
                if (playersManager.healNum > 0)
                {
                    Oxygen += playersManager.healValue;
                    oxygenSlider.value = Oxygen;
                    Instantiate(playersManager.healEffect, transform.position, transform.rotation);

                    if (Oxygen > playersManager.startOxy)
                    {
                        Oxygen = playersManager.startOxy;
                        oxygenSlider.value = Oxygen;
                    }

                    playersManager.healNum--;
                }
            }
            
            
            
        }

        if (robot)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                float playerOx = other.gameObject.GetComponent<Playerstatus>().Oxygen;
                if (robUI)
                {
                    robUI.RobotUITrue(playerOx);
                }

            }
        }
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("DamageArea"))
        {
            soundManager.magmaSoundTiming += Time.deltaTime;
            soundManager.MagmaSound(audioSource);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        
        if (other.gameObject.CompareTag("DamageArea"))
        {
            damageAreaEnter = false;
            damageAreaControl = null;

            soundManager.magmaSoundTiming = 0;

            playersManager.heatEffect.SetActive(false);

        }

        if (other.gameObject == baseArea)
        {
            if (diver || robot)
            {
                baseAreaEnter = false;
                //playersManager.robotUIObject.SetActive(true);
                //playersManager.UI.SetActive(true);
            }
            
        }
        
    }

    void Update()
    {
        if (damageAreaEnter)
        {
            if (oxygenFill)
            {
                oxygenFill.color = new Color(255, 0, 0);
            }
        }
        else
        {
            if (oxygenFill)
            {
                oxygenFill.color = startColor;
            }
        }
        SliderControl(baseAreaEnter, damageAreaEnter);
    }

    void SliderControl(bool baseareaEnter, bool damageAreaEnter)
    {
        if (!baseareaEnter)
        {
            if (!playersManager.pauseUI)
            {
                if (oxygenSlider)
                {
                    if (Oxygen > 0)
                    {
                        if (damageAreaEnter)
                        {
                            Oxygen -= damageAreaControl.DV * OxygenDecreaseRate * Time.deltaTime;
                        }
                        else
                        {
                            Oxygen -= OxygenDecreaseRate * Time.deltaTime;
                            oxygenFill.color = startColor;
                        }
                        oxygenSlider.value = Oxygen;
                    }

                    
                }

                if (enemyEnter)
                {

                    oxygenBackSlider.value -= playersManager.damageSpeed;

                    if (oxygenBackSlider.value < oxygenSlider.value)
                    {
                        oxygenBackSlider.value = oxygenSlider.value;
                        oxygenBackUI.SetActive(false);
                        enemyEnter = false;
                    }

                }

                if (batterySlider)
                {
                    if (Battery > 0)
                    {
                        Battery -= BatteryDecreaseRate * Time.deltaTime;
                        
                        if (playerSearch.tf)
                        {
                            Battery -= playersManager.batteryValue * Time.deltaTime;
                        }
                        batterySlider.value = Battery;
                    }
                }
            }

            if (mainCanvasGroup)
            {
                if (mainCanvasGroup.alpha <= 1)
                {
                    mainCanvasGroup.alpha += playersManager.UIAlphaSpeed * Time.deltaTime;
                }
            }

            if (robotCanvasGroup)
            {
                if(robotCanvasGroup.alpha <= 1)
                {
                    robotCanvasGroup.alpha += playersManager.UIAlphaSpeed * Time.deltaTime;
                }
            }
            

        }
        else
        {
            if (oxygenSlider)
            {
                Oxygen = oxygenSlider.maxValue;
                oxygenSlider.value = Oxygen;
                
            }

            if (batterySlider)
            {
                Battery = batterySlider.maxValue;
                batterySlider.value = Battery;
            }

            if (mainCanvasGroup)
            {
                if (mainCanvasGroup.alpha > 0)
                {
                    mainCanvasGroup.alpha -= playersManager.UIAlphaSpeed * Time.deltaTime;
                }
            }

            if (robotCanvasGroup)
            {
                if (robotCanvasGroup.alpha > 0)
                {
                    robotCanvasGroup.alpha -= playersManager.UIAlphaSpeed * Time.deltaTime;
                }
            }

        }
    }
}
