using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Analysis : MonoBehaviour
{
    public TreasureJudge itempick;
    public Issa_analysis ana;
    public jmp.analysis analysis;

    bool CanAnalysis;

    private void Start()
    {
        itempick = GetComponentInChildren<TreasureJudge>();
        GameObject analysisObject = GameObject.Find("Analysis");

        if (analysisObject)
        {
            analysis = analysisObject.GetComponent<jmp.analysis>();
        }
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Analysis"))
        {
            ana = other.gameObject.GetComponent<Issa_analysis>();

            
            
            /*if (ana != null)
            {
                if (itempick.itemPikking || ana.inventory.items.Count > 0)
                {
                    ana.ReadyAnalyze();
                    CanAnalysis = true;

                }
            }*/
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Analysis"))
        {
            Debug.Log(other.gameObject.name);

            if(ana != null)
            {
                ana.InvalidAnalyze();
                ana = null;
            }
            

            /*if (ana != null)
            {
                ana.InvalidAnalyze();
                CanAnalysis = false;
            }*/
        }
    }
    void OnAnalyze()
    {
        if (CanAnalysis)
        {
            analysis.OnAnalysis();
            if (itempick.itemPikking == true)
            {
                TreasureDetail treasureDetail;
                itempick.pickupTarget = null;
                if (itempick.enterObject)
                {
                    treasureDetail = itempick.enterObject.GetComponent<TreasureDetail>();
                    treasureDetail.analyzed = true;
                    analysis.BigAnalysis(itempick.treasureDetail.Infomation);
                }
                

                
                //itempick.enterObject.SetActive(false);
                
                Time.timeScale = 0f;
                itempick.enterObject = null;
                itempick.itemPikking = false;
                CanAnalysis = false;
                BroadcastMessage("OnPlayerAction", "Analysis");

            }

            if (ana != null)
            {
                foreach(var i in itempick.inventoryObj)
                {
                    TreasureDetail treasureDetail = i.GetComponent<TreasureDetail>();
                    Debug.Log(treasureDetail);
                    treasureDetail.get = false;
                    treasureDetail.analyzed = true;
                    
                }
                ana.inventory.items.Clear();
                itempick.inventoryObj.Clear();
                CanAnalysis = false;
            }
        }
    }

    void FixedUpdate()
    {
        if (ana != null)
        {
            if (itempick.itemPikking || ana.inventory.items.Count > 0)
            {
                ana.ReadyAnalyze();
                CanAnalysis = true;
            }
            else
            {
                ana.InvalidAnalyze();
                CanAnalysis = false;
            }
        }
        else
        {
            CanAnalysis = false;
        }
    }

}
