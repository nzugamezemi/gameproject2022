using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sonar : MonoBehaviour
{
    public Transform target;

    

    Renderer rend;

    public bool visible
    {
        get
        {
            if (!rend)
            {
                rend = GetComponent<Renderer>();
            }

            if (rend)
            {
                return rend.enabled;
            }
            else
            {
                return false;
            }
        }

        set
        {
            if (!rend)
            {
                rend = GetComponent<Renderer>();
            }

            if (rend)
            {
                rend.enabled = value;
            }
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();

        ResetTime();
    }

    // Update is called once per frame
    void Update()
    {
        if (rend && target)
        {
            rend.material.SetVector("_position", target.position);
        }
    }

    public void ResetTime()
    { 
        if (rend)
        {
            rend.material.SetFloat("_startTime", Time.time);
        }
    }
}
