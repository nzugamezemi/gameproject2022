using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSearch : MonoBehaviour
{
    TreasureDetail td;

    float spreadTime = 0;
    public float searchSpeed = 1;
    public float startRadius = 0.01f;
    public float radiusSpeed = 5;
    Vector3 texTransform;

    public Vector3 startSearchAreaScale = Vector3.one;
    public GameObject treasureSearcheffect;
    public GameObject searchTexObject;

    public List<GameObject> treasureSearchObj = new List<GameObject>();

    MeshRenderer rend;
    public SoundManager soundManager;

    public SphereCollider sphereCollider;

    public AudioSource audioSource;
    public int num = 0;

    
    private void Start()
    {
        
        transform.localScale = startSearchAreaScale;

        sphereCollider = GetComponent<SphereCollider>();
        sphereCollider.radius = startRadius;
    }

    // サーチの範囲の判定
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Treasure")
        {
            TreasureEnterArea(other.gameObject);
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Treasure")
        {
            TreasureExitArea(other.gameObject);
        }
    }

    GameObject search;
    // サーチの範囲に入った時の処理
    void TreasureEnterArea(GameObject enterTreasure)
    {
        search = Instantiate(treasureSearcheffect, enterTreasure.transform.position, new Quaternion());
        search.transform.parent = enterTreasure.transform;
        treasureSearchObj.Add(search);
    }
    

    void TreasureExitArea(GameObject exitTreasure)
    {
        if (exitTreasure.transform.childCount > 0)
        {
            int num = exitTreasure.transform.childCount - 1;
            Destroy(exitTreasure.transform.GetChild(num).gameObject);
        }
        
    }

    public bool tf = false;
    public GameObject tex;

    public void SearchTex(bool input)
    {
        
        if (input)
        {
            if(tex == null)
            {
                tex = Instantiate(searchTexObject, transform.position , Quaternion.identity);

                rend = tex.GetComponent<MeshRenderer>();

                soundManager.SearchSound(audioSource, searchSpeed);

                
            }
        }
        else
        {
            
            if (tex != null)
            {
                SearchStop();
            }

            
        }

        tf = input;
    }
    

    private void Update()
    {
        if (tf)
        {
            if (tex != null)
            {
                

                spreadTime += Time.deltaTime;

                if(sphereCollider.radius <= 2)
                {
                    sphereCollider.radius += spreadTime / radiusSpeed;
                }
                
                rend.material.SetFloat("_Timea", spreadTime);
                tex.transform.position = transform.position;
                
            }
        }
    }

    public void SearchStop()
    {
        spreadTime = 0;
        soundManager.StopSearchSound();
        audioSource = null;
        Destroy(tex);
        tex = null;
    }

    private void OnDisable()
    {
        if(tex != null)
        {
            Destroy(tex);
            tex = null;

            
        }

        if(treasureSearchObj != null)
        {
            foreach(var i in treasureSearchObj)
            {
                Destroy(i);
                Debug.Log(i);
            }

            treasureSearchObj.Clear();
        }

        sphereCollider.radius = startRadius;
    }
}
