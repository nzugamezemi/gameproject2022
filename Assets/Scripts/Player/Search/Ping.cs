using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Ping : MonoBehaviour
{
    public Transform target;
    public float cameraDistance = 10;
    public float radius = 0;
    public float speed = 1f;
    public float alpha = 1;

    public float time = 0;
    

    // Start is called before the first frame update
    void Start()
    {
        
        target = transform.parent;

        transform.parent = Camera.main.transform;
        transform.localPosition = new Vector3(0, 0, cameraDistance);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
