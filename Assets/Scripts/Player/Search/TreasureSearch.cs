using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureSearch : MonoBehaviour
{
    float spreadTime = 0;
    Quaternion q;

    MeshRenderer rend;
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<MeshRenderer>();
        q = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        spreadTime += Time.deltaTime;
        rend.material.SetFloat("_Timea", spreadTime);
        transform.rotation = q;
    }
}
