using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSearch2 : MonoBehaviour
{
    TreasureDetail td;

    GameObject searchObject;
    public Material searchEnterChangeColor;
    public List<GameObjectsDetail> gameObjectsDetails = new List<GameObjectsDetail>();

    [SerializeField]Vector3 startSearchAreaScale;

    private void Start()
    {
        searchObject = transform.root.gameObject;
        transform.localScale = startSearchAreaScale;
    }

    // サーチの範囲の判定
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Treasure")
        {
            TreasureEnterArea(other.gameObject);
        }
    }

    // サーチの範囲に入った時の処理
    void TreasureEnterArea(GameObject enterTreasure)
    {
        td = enterTreasure.GetComponent<TreasureDetail>();
        td.god.enterObject = enterTreasure;
        //td.god.startMaterial = enterTreasure.GetComponent<Renderer>().material;
        //td.search.SetActive(true);

        enterTreasure.GetComponent<Renderer>().material = searchEnterChangeColor;
        gameObjectsDetails.Add(td.god);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Treasure")
        {
            TreasureExitArea(other.gameObject);
        }
    }

    void TreasureExitArea(GameObject ExitTreasure)
    {
        //ExitTreasure.GetComponent<Renderer>().material = td.god.startMaterial;
        ExitTreasure.transform.GetChild(0).gameObject.SetActive(false);
        gameObjectsDetails.Remove(td.god);
    }
}
