using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class LookAt : MonoBehaviour
{
    public GameObject target;
    public float rotationSmoothing = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (target)
        {
            Vector3 delta = target.transform.position - transform.position;
            Vector3 direction = delta.normalized;
            float directionRad = Mathf.Atan2(delta.y, delta.x);
            float directionDeg = directionRad * 180f / Mathf.PI;
            //Quaternion targetRotation = Quaternion.Euler(0, 0, directionDeg);

            Vector3 up = new Vector3(delta.y, -delta.x, 0);
            if (up.y < 0)
            {
                up *= -1f;
            }

            Quaternion targetRotation = Quaternion.LookRotation(direction, up);


            transform.rotation = Quaternion.Lerp(targetRotation, transform.rotation, rotationSmoothing);
        }
        
    }
}
