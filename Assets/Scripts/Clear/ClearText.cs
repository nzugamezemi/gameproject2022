using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

using UnityEngine.SceneManagement;

public class ClearText : MonoBehaviour
{
    public string[] Clearsentence;

    int Currentsetence;
    public float delaytime;

    public TextMeshProUGUI text1;
    public TextMeshProUGUI text2;

    public Animator text1_ani;

    public Animator text2_ani;

    public GameObject brackCanvus;
    public BrackOut brackOut;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(TextSet(delaytime));


        brackOut = brackCanvus.GetComponent<BrackOut>();
    }
    IEnumerator TextSet(float delay)
    {
        text1.text = Clearsentence[Currentsetence];

        text1_ani.SetBool("isShown", true);

        Currentsetence += 1;

        yield return new WaitForSeconds(delay);

        if (Currentsetence < Clearsentence.Length)
        {

            text1_ani.SetBool("isShown", false);
            StartCoroutine(NextTextSet(delaytime));

        }
        else
        {
            if (SceneManager.GetActiveScene().name == "Clear")
            {
                Invoke("ClearGame", 3.0f);
                text1_ani.SetBool("isShown", false);
            }

            if (SceneManager.GetActiveScene().name == "Prologue")
            {
                Invoke("MainChange", 3.0f);
                text1_ani.SetBool("isShown", false);
            }
            brackOut.WhitetoBrack();
            //Invoke("ClearGame", 3.0f);
            //text1_ani.SetBool("isShown", false);
            yield break;
        }




    }
    IEnumerator NextTextSet(float delay)
    {

        text2.text = Clearsentence[Currentsetence];
        text2_ani.SetBool("isShown", true);

        Currentsetence += 1;

        yield return new WaitForSeconds(delay);

        if (Currentsetence < Clearsentence.Length)
        {

            text2_ani.SetBool("isShown", false);
            StartCoroutine(TextSet(delaytime));

        }
        else
        {
            if(SceneManager.GetActiveScene().name == "Clear")
            {
                Invoke("ClearGame", 3.0f);
                text2_ani.SetBool("isShown", false);
            }
            
            if(SceneManager.GetActiveScene().name == "Prologue")
            {
                Invoke("MainChange", 3.0f);
                text2_ani.SetBool("isShown", false);
            }

            brackOut.WhitetoBrack();
            yield break;
        }
        
    }

    public void MainChange()
    {
        SceneManager.LoadScene("Main");
    }

    public void ClearGame()
    {

        SceneManager.LoadScene("title scene");
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
