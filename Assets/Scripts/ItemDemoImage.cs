using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;


// 要注意：このクラスはSelectableを継承する！
public class ItemDemoImage : Selectable
{
    // 説明文
    public string Displayname;
    public string infoText;
    public Sprite icon;
    jmp.analysisItem iteminfo;

    // C#のイベント：このオブジェクトが選択されたら送信する
    public Action<ItemDemoImage> OnSelection;

    // 選択されたらUnityにこれが実行される。
    //public override void OnSelect(BaseEventData eventData)
    //{
        
    //    // 通常の動作
    //    base.OnSelect(eventData);

    //    // 終わったら通知を送信する
    //    OnSelection?.Invoke(this);
    //}

}
