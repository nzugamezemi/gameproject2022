using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour
{
    public GameObject door;
    public Vector3 move;
    public Quaternion rotate = Quaternion.Euler(0, 0, 0);
    public float speed = 2f;
    public float moveTime = 5f;
    public bool On = false;


    Player player;
    float time;
    Collider boxCollider;

    void Start()
    {
        player = GameObject.Find("Robot(Clone)").GetComponent<Player>();
        time = moveTime;
        boxCollider = door.GetComponent<BoxCollider>();
    }

    //Vector3 Destination = Door.transform.position.magnitude + Move;

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == LayerMask.NameToLayer("Robot") && player.electricity)
        {
            Debug.Log("すり抜けた！");
            On = true;
        }
    }

    void ToOpen()
    {
        if(On == true)
        {
            boxCollider.enabled = false;
            time -= Time.deltaTime;
            door.transform.position += move * speed * Time.deltaTime;
            door.transform.rotation = door.transform.rotation * rotate;
            if (time <= 0)
            {
                On = false;
                door.SetActive(false);
            }
        }
    }

    void FindPlayer()
    {
        if (player == null)
        {
            player = GameObject.Find("Robot(Clone)").GetComponent<Player>();
        }
    }

    void FixedUpdate()
    {
        ToOpen();

        FindPlayer();
    }

}
