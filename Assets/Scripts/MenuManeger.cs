using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MenuManeger : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject Menuwindow;

    bool menucheck;

    public GameObject BagClose;
    public GameObject BagOpen;
    void Start()
    {
        Menuwindow.SetActive(false);
        menucheck = false;
        BagClose.SetActive(true);
        BagOpen.SetActive(false);
    }

    void OnMenu()
    {
        menucheck = !menucheck;

        Menuwindow.SetActive(menucheck);
        BagClose.SetActive(!menucheck);
        BagOpen.SetActive(menucheck);
    }

    // Update is called once per frame
    void Update()
    {
        //if (!menucheck & Gamepad.current.buttonNorth.wasPressedThisFrame)
        //{
        //Menuwindow.SetActive(true);
        //menucheck = true;

        //BagClose.SetActive(false);
        //BagOpen.SetActive(true);

        //}
        //else if(menucheck & Gamepad.current.buttonNorth.wasPressedThisFrame)
        //{
        //    Menuwindow.SetActive(false);
        //    menucheck = false;

        //    BagClose.SetActive(true);
        //    BagOpen.SetActive(false);
        //}
    }
}
