using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public Transform target;

    Vector3 offset = Vector3.zero;
    RectTransform rt;
    

    // Start is called before the first frame update
    void Start()
    {
        rt = GetComponent<RectTransform>();
        if (!target)
        {
            target = transform.parent;
            transform.parent = transform.parent.parent;
            transform.localEulerAngles = Vector3.zero;
        }

        if (target)
        {

            offset = target.position - transform.position;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (target)
        {
            gameObject.transform.rotation = Quaternion.Euler(Vector3.zero);
            //transform.LookAt(UIcam);
        }
    }
}
