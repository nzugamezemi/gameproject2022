using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class SnakeDemo : MonoBehaviour
{
    public Transform target;
    public float smoothing = 0.5f;

    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            Vector3 direction = (target.position - transform.position).normalized;

            float frontDirectionX = Mathf.Sign(direction.x);
            Vector3 forward = new Vector3(0, 0, frontDirectionX);

            Vector3 up = new Vector3(direction.y * -forward.z, direction.x * forward.z, 0).normalized;

            Debug.DrawRay(transform.position, up * 2f, Color.green);
            Debug.DrawRay(transform.position, forward * 2f, Color.blue);
            rb.MoveRotation(Quaternion.Lerp(Quaternion.LookRotation(forward, up), transform.rotation, smoothing));
        }
        
    }
}
