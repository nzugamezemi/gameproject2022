using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class UICamera : MonoBehaviour
{
    Camera cam;

    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<Camera>();
        Camera parentCam = GetComponentInParent<Camera>();
        if (parentCam != null)
        {
            cam.fieldOfView = parentCam.fieldOfView;
        }

        cam.clearFlags = CameraClearFlags.Depth;
    }


}
