using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BreakBlock : MonoBehaviour
{
    Rigidbody rb;
    FixedJoint joint;

    public List<GameObject> childObjects;

    void Start()
    {
        childObjects = new List<GameObject>();
        for(int i = 0; i < transform.childCount; i++)
        {
            childObjects.Add(transform.GetChild(i).gameObject);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                Destroy(childObjects[i].GetComponent<FixedJoint>());
            }
        }
    }

}
