using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class SaveDetail 
{
    public bool save = false;
    public float oxygen;
    public float battery;
    public Vector3 diverPosition;
    public Vector3 robotPosition;
    public List<bool> inventory;
    public bool[] gimmics;
    public List<Vector3> treasurePosition;
    public List<bool> treasureAnalysis;
}
