using System.IO;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;

public class SaveManager : MonoBehaviour
{
    //SaveDetail save;
    public string filePath;

    public Playerstatus diverstatus;
    public Playerstatus robotstatus;
    public PlayersManager playersManager;
    public jmp.Inventory inventory;

    private void Awake()
    {
        //filePath = Application.dataPath + "/savedate.json";

        //playersManager = GetComponent<PlayersManager>();
        
        /*if (playersManager.inventory)
        {
            inventory = playersManager.inventory.GetComponent<jmp.Inventory>();
        }*/
    }
    private void OnEnable()
    {
        
        //filePath = Application.persistentDataPath + "/savedate.json";
        //save = new SaveDetail();
    }

    public void players(GameObject diver ,GameObject robot)
    {
        diverstatus = diver.GetComponent<Playerstatus>();
        robotstatus = robot.GetComponent<Playerstatus>();
    }

    public void Save()
    {
        

        SaveDetail save = new SaveDetail();

        SaveDate(save);

        string json = JsonUtility.ToJson(save);
        //File.WriteAllText(filePath, json);
        StreamWriter streamWriter = new StreamWriter(filePath);
        streamWriter.Write(json);
        streamWriter.Flush();
        streamWriter.Close();

        

        Debug.Log(filePath);
        Debug.Log("save:" + save.oxygen);
        Debug.Log("save:" + save.battery);
        Debug.Log("save:" + save.diverPosition);
        Debug.Log("save:" + save.robotPosition);

        foreach(var i in save.gimmics)
        {
            Debug.Log("save:" + save.gimmics);
        }
       


       
    }

    public void Load()
    {
        if (File.Exists(filePath))
        {
            StreamReader streamReader;
            streamReader = new StreamReader(filePath);
            string data = streamReader.ReadToEnd();
            streamReader.Close();
            //string data = File.ReadAllText(filePath);
            SaveDetail save = JsonUtility.FromJson<SaveDetail>(data);

            LoadDate(save);

            
            
            if (inventory.items.Count > 0)
            {
                foreach (var i in inventory.items)
                {
                    Debug.Log("load:" + i.Key + " " + i.Value[0].type);
                }
            }
            else
            {
                Debug.Log("null");
            }
            
        }
    }

    void SaveDate(SaveDetail save)
    {
        save.save = true;
        save.oxygen = diverstatus.Oxygen;
        save.battery = robotstatus.Battery;
        save.diverPosition = playersManager.Diver.transform.position;
        save.robotPosition = playersManager.Robot.transform.position;

        SaveTreasureAnalysis(save);
        GetInventory(save);

        
        SaveTreasurePosition(save);
        
        SaveGimmics(save);
    }

    void GetInventory(SaveDetail save)
    {
        save.inventory = new List<bool>();

        for (var i = 0; i < playersManager.treasures.Length; i++)
        {
            TreasureDetail treasureDetail = playersManager.treasures[i].GetComponent<TreasureDetail>();
            
            save.inventory.Add(treasureDetail.get);
        }

    }

    void SaveTreasurePosition(SaveDetail save)
    {
        save.treasurePosition = new List<Vector3>();

        for(var i = 0; i < playersManager.treasures.Length; i++)
        {
            save.treasurePosition.Add(playersManager.treasures[i].transform.position);
        }
    }

    void SaveTreasureAnalysis(SaveDetail save)
    {
        save.treasureAnalysis = new List<bool>();

        for (var i = 0; i < playersManager.treasures.Length; i++)
        {
            TreasureDetail treasureDetail = playersManager.treasures[i].GetComponent<TreasureDetail>();

            save.treasureAnalysis.Add(treasureDetail.analyzed);
        }
    }

    void SaveGimmics(SaveDetail save)
    {

        save.gimmics = new bool[playersManager.gimmics.Count];
        for(var i = 0; i < playersManager.gimmics.Count; i++)
        {
            Gimmicks gimmicks = playersManager.gimmics[i].GetComponent<Gimmicks>();
            save.gimmics[i] = gimmicks.excution;
        }
    }

    void LoadDate(SaveDetail save)
    {
        if (save.save)
        {
            diverstatus.Oxygen = save.oxygen;
            Debug.Log("load:" + save.oxygen);
            robotstatus.Battery = save.battery;
            Debug.Log("load:" + save.battery);
            playersManager.Diver.transform.position = save.diverPosition;
            playersManager.Robot.transform.position = save.robotPosition;

            
            LoadTreasurePosition(save);
            LoadTreasureAnalysis(save);
            LoadInventory(save);
            LoadGimmic(save);

            
            
            Debug.Log("load:" + save.diverPosition);
            Debug.Log("load:" + save.robotPosition);
        }
        
        
    }

    void LoadInventory(SaveDetail save)
    {
        TreasureJudge treasureJudge = playersManager.treasureJudge;
        for (var i = 0; i < save.inventory.Count; i++)
        {
            TreasureDetail treasureDetail = playersManager.treasures[i].GetComponent<TreasureDetail>();
            

            treasureDetail.get = save.inventory[i];
            if (treasureDetail.get)
            {
                

                if (!inventory.items.ContainsKey(treasureDetail.Infomation.displayName))
                {
                    inventory.items[treasureDetail.Infomation.displayName] = new List<jmp.ItemInfo>();
                }
                inventory.items[treasureDetail.Infomation.displayName].Add(treasureDetail.Infomation);

                treasureJudge.inventoryObj.Add(playersManager.treasures[i]);
            }
        }
    }

    void LoadTreasurePosition(SaveDetail save)
    {
        for(var i = 0; i < save.treasurePosition.Count; i++)
        {
            playersManager.treasures[i].transform.position = save.treasurePosition[i];
        }
    }

    void LoadTreasureAnalysis(SaveDetail save)
    {
        for (var i = 0; i < save.treasureAnalysis.Count; i++)
        {
            TreasureDetail treasureDetail = playersManager.treasures[i].GetComponent<TreasureDetail>();

            treasureDetail.analyzed = save.treasureAnalysis[i];
        }
    }

    void LoadGimmic(SaveDetail save)
    {
        for(var i = 0; i < playersManager.gimmics.Count(); i++)
        {
            
            Gimmicks gimmicks = playersManager.gimmics[i].GetComponent<Gimmicks>();
            gimmicks.excution = save.gimmics[i];

            if (gimmicks.excution)
            {
                playersManager.gimmics[i].SetActive(false);
            }

            
        }
    }
}
