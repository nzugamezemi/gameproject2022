using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDestroy : MonoBehaviour
{
    private float destroy = 8;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        destroy -= Time.deltaTime;
        if(destroy < 0)
        {
            Destroy(this.gameObject); //衝突したゲームオブジェクトを削除
        }
    }
}
