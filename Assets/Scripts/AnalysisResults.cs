using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class AnalysisResults : MonoBehaviour
{
   // public string text;
    //public TextMeshProUGUI label;
    public Image image;

    Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void ShowUI(bool val)
    {
        if (animator != null)
        {
            animator.SetBool("Visible", val);
        }
    }

    //public void ShowResults()
    //{
    //    if (label)
    //    {
    //        label.text = text;
    //    }
    //}
}
