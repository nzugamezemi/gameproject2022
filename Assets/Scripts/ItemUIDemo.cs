using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ItemUIDemo : MonoBehaviour
{

    public Selectable defaultSelection;

    public TextMeshProUGUI infoLabel;
    public TextMeshProUGUI DisplayName;
    public Image icon;


    private void OnEnable()
    {
        if (defaultSelection)
        {
            defaultSelection.Select();

        }
       
    }
    public void set()
    {
        foreach (jmp.analysisItem item in GetComponentsInChildren<jmp.analysisItem>())
        {
            item.OnSelection += ItemSelected;
        }
    }
    
    // Start is called before the first frame update
    private void Start()
    {
        
    }
    private void OnDisable()
    {
        foreach (jmp.analysisItem item in GetComponentsInChildren<jmp.analysisItem>())
        {
            item.OnSelection -= ItemSelected;
        }
    }

    void ItemSelected(jmp.analysisItem item)
    {
        if (infoLabel)
        {
            infoLabel.text = item.displayname;
        }
        if (DisplayName)
        {
            DisplayName.text = item.iteminfomation;

        }
        if (icon)
        {
            icon.sprite = item.anaicon.sprite;
        }
        Debug.Log(DisplayName.text);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
