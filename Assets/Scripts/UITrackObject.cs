using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITrackObject : MonoBehaviour
{
    public GameObject trackedObject;
    public Vector3 offset;
    public Vector2 margin = new Vector2(200, 200);
    public float flipMarginTop = 300;
    public float flipSmooth = 0.95f;


    RectTransform rt;
    Vector3 targetOffset;

    // Start is called before the first frame update
    void Start()
    {
        rt = GetComponent<RectTransform>();
        targetOffset = offset;
    }

    // Update is called once per frame
    void Update()
    {
        if (trackedObject != null)
        {
            Vector3 uiOffset = offset;
            Vector3 objectScreenPosition = Camera.main.WorldToScreenPoint(trackedObject.transform.position);
            if (objectScreenPosition.y > Camera.main.pixelHeight - flipMarginTop)
            {
                uiOffset.y *= -1f;
            }
            targetOffset = Vector3.Lerp(uiOffset, targetOffset, flipSmooth);

            Vector3 targetPosition = Camera.main.WorldToScreenPoint(trackedObject.transform.position + targetOffset);

            Vector3 selfScreenPosition = Camera.main.WorldToScreenPoint(transform.position);

            objectScreenPosition.x = Mathf.Clamp(targetPosition.x, margin.x, Camera.main.pixelWidth - margin.x);
            objectScreenPosition.y = Mathf.Clamp(targetPosition.y, margin.y, Camera.main.pixelHeight - margin.y);

            objectScreenPosition.z = selfScreenPosition.z;

            Vector3 newPosition = Camera.main.ScreenToWorldPoint(objectScreenPosition);
            rt.position = newPosition;
        }
    }
}
