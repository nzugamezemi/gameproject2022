using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishsMovement : MonoBehaviour
{
    public float speed = 1;
    public float angle = 0;
    // Start is called before the first frame update
    public bool frontX;
    public bool frontY;
    public bool frontZ;
    void Start()
    {
        angle = Random.Range(0, 360);

        if(angle < 90)
        {
            transform.Rotate(angle, 0, 0);
        }
        else
        {
            transform.Rotate(angle, 0, 180);
        }
         
    }

    // Update is called once per frame
    void Update()
    {
        if (frontX)
        {
            transform.position += transform.right * speed * Time.deltaTime;
        }
        else if(frontY){
            transform.position += transform.up * speed * Time.deltaTime;
        }
        else
        {
            transform.position += transform.forward * speed * Time.deltaTime;
        }
        
    }
}
