using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TrackPlayers))]
public class Utsubo : MonoBehaviour
{
    public float maxDistance = 10f;
    public float fieldOfView = 0.2f;
    public float minDistance = 0.5f;
    public float returnSpeed = 1.0f;
    // public float minTargetDistance = 2f;

    public float attackSpeed = 5f;
    public float chaseMinTime = 2f;
    float chaseTime = 0;
    public float deceleration = 2f;
    public float aim = 3f;
    public float smoothing = 0.9f;
   // float speed = 0f;
    public float aimTime = 0f;
    Rigidbody rb;
    public Animator headAnimator;
    public GameObject light;
    public bool blind = false;
    bool inBase = false;
    new MeshRenderer renderer;
    public List<Vector3> positions = new List<Vector3>();

    AudioSource audioSource;
    SoundManager soundManager;

    public enum States
    {
        Idle,
        Aim,
        Chase,
        Wait,
        Return
    }

    public States state = States.Idle;

    PlayersManager playersManager;

    TrackPlayers trackPlayers;

    public void Start()
    {
        rb = GetComponent<Rigidbody>();
        trackPlayers = GetComponent<TrackPlayers>();
        rb.drag = 100;

        //light = GameObject.Find("Spot Light");
        light = GameObject.Find("Robot(Clone)").transform.Find("Spot Light").gameObject;

        // Z軸の移動がない
        //rb.constraints = RigidbodyConstraints.FreezePositionZ;

        home = transform.Find("Home");
        home.parent = transform.parent;

        idleTarget = transform.Find("IdleTarget");
        idleTarget.parent = transform.parent;

        attackTarget = transform.Find("AttackTarget");
        attackTarget.parent = transform.parent;


        audioSource = GetComponent<AudioSource>();
        GameObject players = GameObject.Find("Players");
        playersManager = players.GetComponent<PlayersManager>();
        soundManager = players.GetComponent<SoundManager>();
        
        
       
        aimTime = aim;
    }

    

    public Transform target = null;
    public Transform attackTarget = null;
    public Transform idleTarget = null;
    public Transform home = null;
    public Transform attackPlayer = null;
    public bool targetVisible = false;


    void MoveTo(Vector3 pos)
    {
        this.transform.LookAt(pos);
        Vector3 direction = (pos - transform.position).normalized;
        rb.velocity = direction * returnSpeed;

        //transform.position += direction * returnSpeed * Time.fixedDeltaTime;
    }

    bool IsHome()
    {
        return (home.position - transform.position).magnitude < 0.1f;
    }

    void StartChase()
    {
        this.tag = "Enemy";
        state = States.Chase;
        chaseTime = 0;
        //state = States.Return;
        Debug.Log("Chase!");

        if (attackTarget != null)
        {
             /*Vector3 vector3 = target.transform.position - this.transform.position;
             Quaternion quaternion = Quaternion.LookRotation(vector3);
             this.transform.rotation = Quaternion.Slerp(this.transform.rotation, quaternion, 0.03f;*/

            rb.AddForce((attackTarget.position - transform.position).normalized * attackSpeed, ForceMode.VelocityChange);
        }
    }

    void IdleUpdate()
    {
        // 行動：アイドリングなら何もしない
        rb.drag = 100;

        target = idleTarget;

        rb.velocity = Vector3.zero;

        // ステートの変更
        if (!blind)
        {
            if (targetVisible && !inBase)
            {
                state = States.Aim;
                aimTime = aim;
            }
        }
        else
        {
            state = States.Wait;
        }
        
    }

    void AimUpdate()
    {
        rb.drag = 100;
        target = attackTarget;

        soundManager.UtuboSound(audioSource);

        aimTime -= Time.deltaTime;
        if(aimTime <= 0f)
        {
            rb.drag = 0.1f;
            StartChase();
        }
        Vector3 vector3 = attackTarget.transform.position - this.transform.position;
        Quaternion quaternion = Quaternion.LookRotation(vector3);
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, quaternion, 0.03f);
    }

    bool Chase = false;

    void ChaseUpdate()
    {
        if(chaseTime == 0f)
        {
            positions.Add(transform.position);
        }

        target = attackTarget;
        Chase = true;

        // ステートの変更
        if (!targetVisible && chaseTime > chaseMinTime)
        {
            this.tag = "Untagged";
            state = States.Return;
            Chase = false;
            Debug.Log("Return!");
        }

        chaseTime += Time.fixedDeltaTime;
        target.position = transform.position + rb.velocity.normalized * 10f;
    }

    void WaitUpdate()
    {
        rb.drag = 3;

        if (light != null)
        {
            if (light.activeSelf == false)
            {
                blind = false;
                state = States.Idle;
            }
            Debug.Log("Wait");
        }
    }

    void FindLight()
    {
        if(light == null)
        {
            light = GameObject.Find("Robot(Clone)").transform.Find("Spot Light").gameObject;
        }
    }

    void ReturnUpdate()
    {
        if(target != home)
        {
            rb.drag = 100;
            target = home;
        }
        else
        {
            rb.drag = 0;
        }

        int i = positions.Count - 1;
        if (i + 1 > 0)
        {
            MoveTo(positions[i]);
        }

        if (positions.Count != 0)
        {
            if ((positions[i] - transform.position).magnitude < 0.1f)
            {
                positions.Remove(positions[i]);
                if (i > 0)
                {
                    i--;
                }
            }
        }
        else
        {
            if (!IsHome())
            {
                MoveTo(home.position);
            }
        }

        // 行動
        //MoveTo(home.position);


        // ステートの変更
        if (targetVisible && !inBase)
        {
            state = States.Aim;
            aimTime = aim;
            // StartChase();
        }
        else if (IsHome())
        {
            state = States.Idle;
            Debug.Log("Idle");
        }
    }

    public GameObject hitObject;
    //public GameObject players;

    void FindAttackTarget()
    {
        trackPlayers.UpdatePlayerList();
        GameObject player = trackPlayers.GetDiver();
        if (player)
        {
            attackTarget.position = player.transform.position;
            attackPlayer = player.transform;
        }

        /*GameObject closest = trackPlayers.GetClosestPlayer();
        if (closest)
        {
            attackTarget.position = closest.transform.position;
            attackPlayer = closest.transform;
        }*/

    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Diver(Clone)")
        {
            this.tag = "Untagged";
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == light)
        {
            blind = true;
            Debug.Log("眩しい");
        }

        if (other.gameObject.name == "BaseArea")
        {
            inBase = true;
            this.tag = "Untagged";
            state = States.Return;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == light)
        {
            blind = false;
            state = States.Idle;
            Debug.Log("眩しくない");
        }

        if (other.gameObject.name == "BaseArea")
        {
            inBase = false;
        }
    }

    void FixedUpdate()
    {
        FindLight();

        if (playersManager.destroyPlayer)
        {
            //playerObjects.Clear();
            hitObject = null;
            playersManager.destroyPlayer = false;
        }


        // 最も近いプレーヤーを探す
        if (state != States.Chase)
        {
            FindAttackTarget();
        }

        if(blind == true)
        {
            state = States.Wait;
        }


        // プレーヤーが見えるか
        if (attackPlayer != null)
        {
            targetVisible = false;

            RaycastHit hit;
            Vector3 forward = transform.TransformDirection(Vector3.forward);
            Debug.DrawRay(transform.position, forward * 10f, Color.green, 0, false);

            Vector3 targetDirection = (attackTarget.position - transform.position).normalized;

            float dot = Vector3.Dot(forward, targetDirection);
            targetVisible = dot > 1f - fieldOfView;

            if (targetVisible)
            {
                if (Physics.Raycast(transform.position, targetDirection, out hit, maxDistance))
                {
                    targetVisible = hit.transform == attackPlayer;
                    hitObject = hit.transform.gameObject;
                }
                else
                {
                    targetVisible = false;
                    hitObject = null;
                }
            }

            Debug.DrawRay(transform.position, targetDirection * maxDistance, targetVisible ? Color.red : Color.blue);


         
        }

        switch (state)
        {
            case States.Idle:
                IdleUpdate();
                break;
            case States.Aim:
                AimUpdate();
                break;
            case States.Chase:
                ChaseUpdate();
                break;
            case States.Wait:
                WaitUpdate();
                break;
            case States.Return:
                ReturnUpdate();
                break;
        }

        {
            // 蛇の動き
            Vector3 direction = (target.position - transform.position).normalized;
            float frontDirectionX = Mathf.Sign(direction.x);
            Vector3 forward = new Vector3(0, 0, frontDirectionX);
            Vector3 up = new Vector3(direction.y * -forward.z, direction.x * forward.z, 0).normalized;
            Quaternion targetRotation = Quaternion.LookRotation(forward, up);
            targetRotation *= Quaternion.Euler(0, 90, 0);

            Debug.DrawRay(transform.position, up * 2f, Color.yellow, 0, false);
            Debug.DrawRay(transform.position, forward * 2f, Color.magenta, 0, false);
            rb.MoveRotation(Quaternion.Lerp(targetRotation, transform.rotation, smoothing));
        }
        

        // アニメーションの切り替え
        if (state == States.Chase || state == States.Aim)
        {
            headAnimator.SetBool("IsAttack", true);
        }
        else
        {
            headAnimator.SetBool("IsAttack", false);
        }
    }
}
