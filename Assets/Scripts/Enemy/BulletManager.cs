using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletManager : MonoBehaviour
{
    float destroy = 0f;
    float destroyTime = 2.0f;

   public void Shoot(Vector3 dir)
    {
        GetComponent<Rigidbody>().AddForce(dir);
    }

    void Update()
    {
        destroy += Time.deltaTime / destroyTime;
        if (destroy >= 1)
        {
            Destroy(this.gameObject);
        }
    }
}
