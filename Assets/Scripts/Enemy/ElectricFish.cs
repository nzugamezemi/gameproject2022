using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricFish : MonoBehaviour
{
    public float maxDistance = 2f;
    public float speed = 1.0f;
    public float Charge = 2f;
    public float DisCharge = 2f;
    public float maxRotation = 0.5f;
    public float maxSpeedY= -0.5f;
    public float maxSpeedX = 2;
    public SphereCollider sc;
    public Light light;
   // public ParticleSystem particle;

     float Caution = 0;
     float dc = 0;

    GameObject eleParticle;
    Vector3 home = Vector3.zero;
    Rigidbody rb;

    new MeshRenderer renderer;

    public enum States
    {
        Idle,
        DisCharge
    }

    public States state = States.Idle;

    PlayersManager playersManager;

    public List<GameObject> p = new List<GameObject>();

    public void Start()
    {
        //sc.enabled = false;
        //light.enabled = false;
        
        //home = transform.position;
        rb = GetComponent<Rigidbody>();

        Caution = Charge;
        dc = DisCharge;

        playersManager = GameObject.Find("Players").GetComponent<PlayersManager>();
        players = playersManager.Diver;
        p.Add(players);

        eleParticle = transform.GetChild(1).gameObject;
        eleParticle.SetActive(false);
    }

    Transform target = null;
    bool targetVisible = false;

    void IdleUpdate()
    {
        // 行動：アイドリングなら何もしない

        // ステートの変更
        if (targetVisible)
        {
            state = States.DisCharge;
            //Debug.Log("Charge");
        }

    }

    void DisChargeUpdate()
    {
        //エフェクトの再生を開始
        eleParticle.SetActive(true);
        // 行動
        if (target != null)
        {
            // 放電するまでの猶予時間
            Caution -= Time.deltaTime;
            light.enabled = true;

            //　Caution が0になったら放電して攻撃する
            if (Caution <= 0)
            {
                this.tag = "Enemy";
                sc.enabled = true;

                dc -= Time.deltaTime;
                if (dc <= 0)
                {
                    this.tag = "Pickup";
                    sc.enabled = false;
                    light.enabled = false;
                  
                    Caution = Charge;
                    dc = DisCharge;
                }
            }
        }

        // ステートの変更
        if (!targetVisible)
        {
            sc.enabled = false;
            light.enabled = false;
          

            state = States.Idle;
            Debug.Log("Idle");
           
        }
    }

    

    public GameObject hitObject;
    public GameObject players;

    void Update()
    {
        if (playersManager.destroyPlayer)
        {
            players = null;
            p.Clear();
            hitObject = null;
        }

        if (players == null)
        {
            players = playersManager.Diver;
            if (p.Count == 0)
            {
                p.Add(players);
            }

        }

        if (p != null)
        {
            float minDistance = 0;
            foreach (GameObject o in p)
            {
                if (o == null)
                {
                    break;
                }
                Vector3 delta = o.transform.position - transform.position;
                if (target == null || minDistance > delta.magnitude)
                {
                    minDistance = delta.magnitude;
                    target = o.transform;
                }
            }
        }



        // 最も近いプレーヤーを探す


        // プレーヤーが見えるか
        if (target != null)
        {
            RaycastHit hit;
            Debug.DrawRay(transform.position, (target.position - transform.position).normalized * maxDistance, Color.red);

            if (Physics.Raycast(transform.position, (target.position - transform.position).normalized, out hit, maxDistance))
            {

                targetVisible = hit.transform == target;
                hitObject = hit.transform.gameObject;
                
            }
            else
            {
                targetVisible = false;
                hitObject = null;
            }
        }

        switch (state)
        {
            case States.Idle:
                IdleUpdate();
                break;
            case States.DisCharge:
                DisChargeUpdate();
                break;
        }
    }

    void FixedUpdate()
    {
        if(Mathf.Abs(rb.rotation.z) > maxRotation)
        {
            rb.angularVelocity -= rb.angularVelocity * 0.01f;
        }

        Vector2 movementDirection = Vector2.zero;
        if (Mathf.Abs(rb.velocity.x) > maxSpeedX)
        {
            rb.velocity = movementDirection;
            //Debug.Log("減速");
        }

        if (Mathf.Abs(rb.velocity.y) > maxSpeedY)
        {
            rb.velocity = movementDirection;
            //Debug.Log("減速");
        }
    }
}
