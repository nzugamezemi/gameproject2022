using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(TrackPlayers))]
public class Enemy : MonoBehaviour
{
    public float maxDistance = 2f;
    public float speed = 1.0f;
    public float memory = 2f; //追いかけているプレイヤーの位置を更新する時間
    public float wayHome = 5f; //帰路を記録するまでの時間

    public float idleReturnSmoothing = 0.99f;
    public Vector3 idleRotation = Vector3.zero;
    Vector3 lastPos = Vector3.zero;

    bool inBase = false;
    public bool checkSwitch = false;

    int i = 0; //記憶した帰路のポイントの数
    float memoryTime = 0f;
    float wayHomeTime = 0f;
    public float checkTime = 0f; //引っかかっていないかを判定するまでの時間
   
    Vector3 home = Vector3.zero;

    new MeshRenderer renderer;

    public enum States
    {
        Idle,
        Chase,
        Wait,
        Search,
        Return
    }

    States state = States.Idle;

    PlayersManager playersManager;

    public List<GameObject> p = new List<GameObject>();
    public List<Vector3> positions = new List<Vector3>();

    public GameObject hitObject;
    public GameObject[] players;
    TrackPlayers trackPlayers;
    Player player;
    Rigidbody rb;

    public Vector3 targetP = Vector3.zero;
    public Vector3 memoryP = Vector3.zero;

    public void Start()
    {
        home = transform.position;
        memoryTime = memory;
        wayHomeTime = wayHome;
        rb = GetComponent<Rigidbody>();

        renderer = transform.GetChild(1).GetComponent<MeshRenderer>();
        renderer.sharedMaterial.EnableKeyword("_EMISSION");
        renderer.material.SetColor("_EmissionColor", new Color(0f , 1f * 100, 0f));

        player = GameObject.Find("Robot(Clone)").GetComponent<Player>();
        playersManager = GameObject.Find("Players").GetComponent<PlayersManager>();
        trackPlayers = GetComponent<TrackPlayers>();

        players = GameObject.FindGameObjectsWithTag("Player");
        foreach(var i in players)
        {
            p.Add(i);
        }
    }

    public Transform target = null;
    
    bool targetVisible = false;

    void MoveTo(Vector3 pos)
    {
        this.transform.LookAt(pos);
        transform.position += transform.forward * speed * Time.deltaTime;
    }

    bool IsHome()
    {
        return (home - transform.position).magnitude < 0.1f;
    }

    void IdleUpdate()
    {
        // 行動：アイドリングなら何もしない

        rb.velocity = Vector3.zero;
        Vector3 forward = new Vector3(Mathf.Sign(transform.forward.z), 0, 0);
        Vector3 up = new Vector3(-Mathf.Sign(forward.z), 0, 0);
        rb.MoveRotation(Quaternion.Lerp(Quaternion.LookRotation(forward, up) * Quaternion.Euler(idleRotation), transform.rotation, idleReturnSmoothing));

        // ステートの変更
        if (targetVisible)
        {
            state = States.Chase;
            //Debug.Log("Chase!");
        }

        renderer.material.SetColor("_EmissionColor", new Color(0f, 1f * 100, 0f));
       
    }

    void ChaseUpdate()
    {
        // 行動
        if (target != null)
        {
            MoveTo(target.position);
            memoryTime -= Time.deltaTime;
            if(memoryTime <= 0)
            {
                memoryP = targetP;
                memoryTime = memory;
            }
        }

        // ステートの変更
        if (!targetVisible)
        {
            state = States.Search;
            //state = States.Return;
            // Debug.Log("Return!");
        }

        if(wayHomeTime > 0)
        {
            wayHomeTime -= Time.deltaTime;

            if (wayHomeTime <= 0)
            {
                positions.Add(transform.position);
                wayHomeTime = wayHome;
            }
        }

        renderer.material.SetColor("_EmissionColor", new Color(1f * 100, 0f, 0f));
        //renderer.material.color = chaseColor;
    }

    void SearchUpdata()
    {
        if (memoryP != null)
        {
            MoveTo(memoryP);
        }
        else
        {
            checkTime = 5f;
            checkSwitch = false;
            state = States.Return;
        }

        if(targetVisible && !inBase)
        {
            state = States.Chase;
        }

        if ((memoryP - transform.position).magnitude < 0.1f)
        {
            checkTime = 5f;
            checkSwitch = false;
            state = States.Return;
        }
    }

    void IdleFromWait()
    {
        rb.drag = 0;
        this.tag = "Enemy";
        checkTime = 5f;
        checkSwitch = false;
        state = States.Return;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "BaseArea")
        {
            inBase = false;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if(state == States.Chase && collision.gameObject.name == "Diver(Clone)")
        {
            Invoke("IdleFromWait", 3);
            rb.drag = 100;
            state = States.Wait;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        //if (state == States.Chase && other.tag == "Robot")
        if (state == States.Chase && other.gameObject.layer == LayerMask.NameToLayer("Electricity"))
        {
            Invoke("IdleFromWait", 3);
            rb.drag = 100;
            state = States.Wait;
        }

        if (other.gameObject.name == "BaseArea")
        {
            inBase = true;
            checkTime = 5f;
            checkSwitch = false;
            state = States.Return;
        }
    }

    void WaitUpdate()
    {
        Debug.Log("Wait");
        renderer.material.SetColor("_EmissionColor", new Color(0f, 1f * 100, 0f));
        this.tag = "Untagged";
    }

    void ReturnUpdate()
    {
        // 行動
        i = positions.Count - 1;
        if (i + 1 > 0)
        {
            MoveTo(positions[i]);
        }

        if (positions.Count != 0)
        {
            if ((positions[i] - transform.position).magnitude < 0.1f)
            {
                positions.Remove(positions[i]);
                if (i > 0)
                {
                    i--;
                }
            }
        }
        else
        {
            if (!IsHome())
            {
                MoveTo(home);
            }
        }

        checkTime -= Time.deltaTime;
        if(checkTime <= 0)
        {
            checkPos();
        }
        //MoveTo(home);

        // ステートの変更
        if (targetVisible && !inBase)
        {
            state = States.Chase;
            // Debug.Log("Chase!");
        }
        else if (IsHome())
        {
            state = States.Idle;
            //Debug.Log("Idle");
        }

        renderer.material.SetColor("_EmissionColor", new Color(0f, 1f * 40, 0f));
        //renderer.material.color = returnColor;
    }

    void FindAttackTarget()
    {
        trackPlayers.UpdatePlayerList();
        GameObject closest = trackPlayers.GetClosestPlayer();
        if (closest)
        {
            target = closest.transform;
        }
        targetP = target.position;
    }

    void checkPos()
    {
        if(checkSwitch == true)
        {
            if((lastPos - transform.position).magnitude < 0.2f && positions.Count != 0)
            {
                positions.Remove(positions[i]);
                if (i > 0)
                {
                    i--;
                }
            }
            checkTime = 5f;
            checkSwitch = false;
        }
        else
        {
            lastPos = transform.position;
            checkTime = 5f;
            checkSwitch = true;
        }
    }

    void Update()
    {
        FindAttackTarget();

        // 最も近いプレーヤーを探す

        // プレーヤーが見えるか
        if (target != null)
        {
            RaycastHit hit;
            Debug.DrawRay(transform.position, (target.position - transform.position).normalized * maxDistance, Color.blue);
            if (Physics.Raycast(transform.position, (target.position - transform.position).normalized, out hit, maxDistance))
            {
                targetVisible = hit.transform == target;
                hitObject = hit.transform.gameObject;
            }
            else
            {
                targetVisible = false;
                hitObject = null;
            }
        }

        switch(state)
        {
            case States.Idle:
                IdleUpdate();
                break;
            case States.Chase:
                ChaseUpdate();
                break;
            case States.Wait:
                WaitUpdate();
                break;
            case States.Search:
                SearchUpdata();
                break;
            case States.Return:
                ReturnUpdate();
                break;
        }
    }
}
