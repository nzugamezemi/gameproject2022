using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootEnemy : MonoBehaviour
{
    public float maxDistance = 5f;
    public GameObject Bullet;

    // Transform myTransform = this.transform;

    new Renderer renderer;

    Color idleColor = new Color(158f / 255f, 242f / 255f, 255f / 255f);
    Color chaseColor = new Color(242f / 255f, 93f / 255f, 24f / 255f);
    Color returnColor = new Color(16f / 255f, 143f / 255f, 31f / 255f);

    enum States
    {
        Idle,
        Shoot
    }

    States state = States.Idle;


    public void Start()
    {
        renderer = GetComponent<Renderer>();

    }

    GameObject[] players = null;

    Transform target = null;
    bool targetVisible = false;

    float loading = 0; // 装填 0〜1;
    float loadingTime = 3f; // 装填にかかる時間

    void IdleUpdate()
    {
        // 行動：アイドリングなら何もしない

        // ステートの変更
        if (targetVisible)
        {
            state = States.Shoot;
            Debug.Log("loading!");
        }

        renderer.material.color = idleColor;
    }

    void ShootUpdate()
    {
        // 行動
        if (target != null)
        {
            // MoveTo(target.position);
            loading += Time.deltaTime / loadingTime;
            if (loading >= 1)
            {
                GameObject bullet = Instantiate(Bullet, new Vector3(10, -3, 0), Quaternion.identity) as GameObject;
                bullet.GetComponent<BulletManager>().Shoot(new Vector3(target.position.x * 500, target.position.y * 50, 0));
                

                Debug.Log(target.position.x);
                loading = 0;
            }

        }


        // ステートの変更
        if (!targetVisible)
        {
            state = States.Idle;
            Debug.Log("Idle");
            loading = 0;
        }

        renderer.material.color = chaseColor;
    }

    public GameObject hitObject;

    void Update()
    {
        if (players == null || players.Length == 0)
        {
            players = GameObject.FindGameObjectsWithTag("Player");
        }

        // 最も近いプレーヤーを探す
        float minDistance = 0;
        if (players != null)
        {
            foreach (GameObject o in players)
            {
                Vector3 delta = o.transform.position - transform.position;
                if (target == null || minDistance > delta.magnitude)
                {
                    minDistance = delta.magnitude;
                    target = o.transform;
                }
            }
        }

        // プレーヤーが見えるか
        if (target != null)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, (target.position - transform.position).normalized, out hit, maxDistance))
            {
                targetVisible = hit.transform == target;
                hitObject = hit.transform.gameObject;
            }
            else
            {
                targetVisible = false;
            }
        }

        switch (state)
        {
            case States.Idle:
                IdleUpdate();
                break;
            case States.Shoot:
                ShootUpdate();
                break;
        }
    }
}
