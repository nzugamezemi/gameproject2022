using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 他のクラス名とかぶらないようにnamespaceを使う。
namespace jmp
{
    // このクラスはアイテムの情報を記憶する。
    [System.Serializable]
    public class ItemInfo
    {
        public string type;
        public string displayName;
        public Sprite sprite;
        public Sprite ana_UI;
        public string itemtext1;
        public string itemtext2;
        public string itemtext3;
        public string itemtext4;
        public string itemtext5;
        public int itemNum;

        [HideInInspector]
        public float pickupTime = 0;
    }
}

