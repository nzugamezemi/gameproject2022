using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace jmp
{
    public class InventoryView : MonoBehaviour
    {
        public jmp.Inventory inventory;
        public GameObject itemViewPrefab;

        // Start is called before the first frame update
        void Start()
        {
            
        }

        private void OnEnable()
        {
            if (inventory)
            {
                // インベントリ変更の通知を受け取る
                inventory.OnInventoryChange += InventoryChanged;

                InventoryChanged();
            }
        }

        private void OnDisable()
        {
            if (inventory)
            {
                // インベントリ変更の通知を受け取らない
                inventory.OnInventoryChange -= InventoryChanged;
            }
        }

        void InventoryChanged()
        {
            // 既存の表示を削除する
            foreach (Transform child in transform)
            {
                Destroy(child.gameObject);
            }

            // アイテムを種類別に並べる場合：
            List<ItemInfo> itemInfos = inventory.items
                .OrderBy(x => x.Key) // ここでアイテム種類の並び変え順を指定する
                .SelectMany(x => x.Value) // ItemInfoだけを取り出す
                .ToList(); // Listに変換

            // アイテムを取得時間で並べる場合：
            //List<ItemInfo> itemInfos = inventory.items
            //    .SelectMany(x => x.Value) // ItemInfoだけを取り出す
            //    .OrderBy(x => x.pickupTime) // 取得時間で並び変える
            //    .ToList(); // Listに変換

            foreach (ItemInfo info in itemInfos)
            {
                GameObject view = Instantiate(itemViewPrefab);
                view.transform.SetParent(transform, false);

                jmp.InventoryItem inventoryItem = view.GetComponent<jmp.InventoryItem>();
                if (inventoryItem)
                {
                    inventoryItem.SetInfo(info);
                }
            }
        }
    }
}

