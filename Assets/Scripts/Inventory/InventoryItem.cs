using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace jmp
{
    public class InventoryItem : MonoBehaviour
    {
       // public TextMeshProUGUI label;
        public Image image;

        public jmp.ItemInfo itemInfo;

        public void SetInfo(jmp.ItemInfo info)
        {
            itemInfo = info;

            //label.text = info.displayName;
            image.sprite = info.sprite;
        }
    }
}


