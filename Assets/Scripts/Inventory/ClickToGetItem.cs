using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace jmp
{
    public class ClickToGetItem : MonoBehaviour
    {
        jmp.Inventory inventory;

        void Start()
        {
            inventory = GetComponent<jmp.Inventory>();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit))
                {
                    jmp.PickupItem pickupItem = hit.collider.gameObject.GetComponent<jmp.PickupItem>();
                    if (pickupItem)
                    {
                        inventory.GetItem(pickupItem.itemInfo);

                        // 取得後、フィールドから削除する
                        Destroy(hit.collider.gameObject);
                    }
                }
            }
        }
    }
}

