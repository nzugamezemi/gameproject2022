using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace jmp
{
    public class ItemCheck : MonoBehaviour
    {
        public jmp.Inventory inventory;
        public jmp.analysis analysis;

        public int itemNumber;

        public GameObject ClearPanel;
        Animator ClearAni;
        // public UnityEvent OnItemGet;
        //public UnityEvent OnItemLose;

        //private void OnEnable()
        //{
        //    if (analysis)
        //    {
        //        インベントリ変更の通知を受け取る
        //        analysis.OnAnalysisChanged += AnalysisChenged;
        //        AnalysisChenged();
        //    }
        //}

        //private void OnDisable()
        //{
        //    if (analysis)
        //    {
        //        インベントリ変更の通知を受け取らない
        //        analysis.OnAnalysisChanged -= AnalysisChenged;
        //    }
        //}

        public void Start()
        {
            if (ClearPanel)
            {
                ClearAni = ClearPanel.gameObject.GetComponent<Animator>();
            }
            

        }
        public void AnalysisCheck()
        {
            if (analysis) 
            {
                if (analysis.Has(itemNumber))
                {
                    Debug.Log("Clear");

                    Time.timeScale = 1f;
                    ClearAni.SetBool("Show", true);
                    Invoke("ClearGame", 3.0f);


                }
                else
                {
                    Time.timeScale = 1f;
                }
                
                //if (inventory.has(itemtype))
                //{
                //    debug.log("clear");
                //    time.timescale = 1f;
                //    clearani.setbool("show", true);
                //    invoke("cleargame", 3.0f);
                //}
                //else
                //{
                //    time.timescale = 1f;
                //}
            }
        }

        public void ClearGame()
        {

            SceneManager.LoadScene("Clear");
        }

       
    }
}

