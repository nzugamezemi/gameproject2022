using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace jmp
{
    public class PickupItem : MonoBehaviour
    {
        public jmp.Inventory inventory;
        public jmp.ItemInfo itemInfo = new jmp.ItemInfo();


        private void Start()
        {
        }
        public void OnTriggerStay(Collider other)
        {
            if(other.gameObject.tag == "Player")
            {
                if (Gamepad.current.buttonNorth.isPressed)
                { 
                    inventory.GetItem(itemInfo);
                    Destroy(gameObject);

                }
            }
        }
    }


    
}

