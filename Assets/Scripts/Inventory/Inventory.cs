using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace jmp
{
    public class Inventory : MonoBehaviour
    {
        public Action OnInventoryChange;

        public Dictionary<string, List<ItemInfo>> items = new Dictionary<string, List<ItemInfo>>();

        public void GetItem(ItemInfo info)
        {
            // 拾った時間を記憶する
            info.pickupTime = Time.time;

            // タイプに合った配列がなければ作っておく
            if (!items.ContainsKey(info.displayName))
            {
                items[info.displayName] = new List<ItemInfo>();
            }
            items[info.displayName].Add(info);

            Debug.Log(items.Count);

            // インベントリの中身が変わった通知を送信する。
            OnInventoryChange?.Invoke();
        }
        public int Count(string displayName)
        {
            if (items.ContainsKey(displayName))
            {
                return items[displayName].Count;
            }
            else
            {
                return 0;
            }
        }

        public bool Has(string itemType)
        {
            return Count(itemType) > 0;
        }
    }
}

