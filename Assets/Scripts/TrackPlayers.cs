using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackPlayers : MonoBehaviour
{
    public List<GameObject> playerObjects = new List<GameObject>();

    private void Start()
    {
        UpdatePlayerList();
    }

    public void UpdatePlayerList()
    {
        playerObjects.Clear();
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject p in players)
        {
            playerObjects.Add(p);
        }
    }

    public void ClearPlayerList()
    {
        playerObjects.Clear();
    }

    private GameObject GetPlayerByLayer(string layer)
    {
        GameObject player = null;
        int layerId = LayerMask.NameToLayer(layer);
        foreach(GameObject go in playerObjects)
        {
            if (go.layer == layerId)
            {
                player = go;
                break;
            }
        }
        return player;
    }

    public GameObject GetDiver()
    {
        return GetPlayerByLayer("Diver");
    }

    public GameObject GetRobot()
    {
        return GetPlayerByLayer("Robot");
    }

    public GameObject GetClosestPlayer()
    {
        GameObject closest = null;
        if (playerObjects.Count > 0)
        {
            float minDistance = 0;
            bool found = false;
            foreach (GameObject o in playerObjects)
            {
                Vector3 delta = o.transform.position - transform.position;
                if (!found || minDistance > delta.magnitude)
                {
                    found = true;
                    minDistance = delta.magnitude;
                    closest = o;
                }
            }
        }

        return closest;
    }
}
