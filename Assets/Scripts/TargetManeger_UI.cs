using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetManeger_UI : MonoBehaviour
{
    public GameObject Target;//UI

    Renderer Targetobj;//確認したいオブジェクト

    // Start is called before the first frame update
    void Start()
    {
        Targetobj = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Targetobj.isVisible)
        {
            Target.SetActive(false);
        }
        else
        {
            Target.SetActive(true);
        }
    }
}
