using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour
{
    public GameObject particleObject;

   // private ParticleSystem particle;

    void Start()
    {
      //  particle = particleObject.GetComponent<ParticleSystem>();
    }

    void OnCollisionEnter(Collision collision)
    {
       // particle.Play();

        Instantiate(particleObject, new Vector3(collision.transform.position.x, this.transform.position.y, collision.transform.position.z - 1), Quaternion.identity); //パーティクル用ゲームオブジェクト生成

       // Debug.Log("砂が巻き上がる");
    }
}
