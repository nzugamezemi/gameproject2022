using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Broken : MonoBehaviour
{
    public string player;
    public List<GameObject> childObjects;
    AudioSource audioSource;

    GameObject players;
    GameObject robot;
    bool isAccele;
    public bool fade;
    SoundManager soundManager;

    BoxCollider boxCollider;

    Gimmicks gimmicks;

    private void Start()
    {
        players = GameObject.Find("Players");
        robot = GameObject.Find("Robot(Clone)");
        soundManager = players.GetComponent<SoundManager>();
        audioSource = GetComponent<AudioSource>();
        boxCollider = GetComponent<BoxCollider>();
        boxCollider.enabled = true;

        gimmicks = GetComponent<Gimmicks>();

        childObjects = new List<GameObject>();
        for (int i = 0; i < transform.childCount; i++)
        {
            childObjects.Add(transform.GetChild(i).gameObject);
        }
    }

    void OnTriggerStay(Collider other)
    {
        if(other.name == "Robot(Clone)" && isAccele)
        {

            for (int i = 0; i < transform.childCount; i++)
            {
                Destroy(childObjects[i].GetComponent<FixedJoint>());
            }

            player = other.name;
            gimmicks.excution = true;

            boxCollider.enabled = false;

            if (soundManager)
            {
                soundManager.BrokenStoneSound(audioSource);
            }

            fade = true;
            Invoke("Destroy", 10);
        }
    }

    void robotAccele()
    {
        if (robot != null)
        {
            isAccele = robot.GetComponent<Player>().isAccele;
        }
        else
        {
            robot = GameObject.Find("Robot(Clone)");
        }
    }

    void FixedUpdate()
    {
        robotAccele();
    }

    void Destroy()
    {
        gameObject.SetActive(false);
    }
}
