using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockContorol : MonoBehaviour
{
    [SerializeField, Range(0f, 0.5f)]
    public float speed_x = 0.5f;
    [SerializeField, Range(0f, 0.5f)]
    public float speed_y = 0.5f;
    [SerializeField, Range(0f, 0.5f)]
    public float speed_z = 0.5f;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(speed_x, speed_y, speed_z);
    }
}
