using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BreakRock : MonoBehaviour
{
    public float fadeSpeed = 0.05f;        // 透明度が変わるスピード
    float red, green, blue, alfa;   // Materialの色

    public Broken broken;
    public bool fade;

   // public bool isFadeOut = false;  // フェードアウト処理の開始、完了を管理
    //public bool isFadeIn = false;   // フェードイン処理の開始、完了を管理

    Renderer fadeMaterial;          // Materialにアクセスする容器

    // Start is called before the first frame update
    void Start()
    {
        fadeMaterial = GetComponent<Renderer>();
        red = fadeMaterial.materials[1].color.r;
        green = fadeMaterial.materials[1].color.g;
        blue = fadeMaterial.materials[1].color.b;
        alfa = fadeMaterial.materials[1].color.a;



    }

    void FixedUpdate()
    {
        fade = broken.fade;
        if (fade == true)
        {
            StartFadeOut(); //boolにチェックが入っていたら実行     
        }
    }

    void StartFadeOut()
    {
        alfa -= fadeSpeed * Time.deltaTime;         // 不透明度を下げる
        SetAlpha();               // 変更した透明度を反映する
        if (alfa <= 0)              // 完全に透明になったら処理を抜ける
        {
            //isFadeOut = false;     // boolのチェックが外れる
            fadeMaterial.enabled = false;  // Materialの描画をオフにする
        }
    }

    void SetAlpha()
    {
        fadeMaterial.materials[0].SetFloat("_Alpha", 0);
        fadeMaterial.materials[1].color = new Color(red, green, blue, alfa);
        // 変更した不透明度を含むMaterialのカラーを反映する
    }
}
