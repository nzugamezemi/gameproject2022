using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameBehavior : MonoBehaviour
{
    private int PlayerHP = 10;

    public int HP
    {
        get
        {
            return PlayerHP;
        }

        set
        {
            PlayerHP = value;
            Debug.LogFormat("Lives:{0}", PlayerHP);
        }
    }

    void OnGUI()
    {
        //GUI.Box(new Rect(20, 20, 150, 25), "プレイヤーのHP:" + PlayerHP);

    }
}
