using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class SoundManager : MonoBehaviour
{
    public AudioClip[] audioClips;

    Coroutine searchCorouine;

    public float magmaSoundTiming;

    public void SearchSound(AudioSource audioSource, float searchspeed)
    {
        audioSource.clip = audioClips[0];
        searchCorouine = StartCoroutine(SearchCoroutine(audioSource, searchspeed));
    }

    public void StopSearchSound()
    {
        StopCoroutine(searchCorouine);
    }

    IEnumerator SearchCoroutine(AudioSource audioSource, float searchspeed)
    {
        while (true)
        {
            if (audioSource != null)
            {
                audioSource.PlayOneShot(audioSource.clip);
            }
            

            yield return new WaitForSeconds(searchspeed);

            
        }
    }

    public void BrokenStoneSound(AudioSource audioSource)
    {
        audioSource.clip = audioClips[2];
        audioSource.PlayOneShot(audioSource.clip);
    }

    public void MagmaSound(AudioSource audioSource)
    {
        audioSource.clip = audioClips[4];
        if(magmaSoundTiming % 6 == 0|| magmaSoundTiming % 10 == 0)
        {
            audioSource.PlayOneShot(audioSource.clip);
        }
        
    }

    public void ItemGetSound(AudioSource audioSource)
    {
        audioSource.clip = audioClips[5];
        audioSource.PlayOneShot(audioSource.clip);
    }

    public void playerDamage(AudioSource audioSource)
    {
        audioSource.clip = audioClips[6];
        audioSource.PlayOneShot(audioSource.clip);
    }

    public void OctopusSound(AudioSource audioSource)
    {
        audioSource.clip = audioClips[7];
        audioSource.PlayOneShot(audioSource.clip);
        audioSource.volume -= 0.05f * Time.deltaTime;
    }

    public void UtuboSound(AudioSource audioSource)
    {
        audioSource.clip = audioClips[8];
        audioSource.PlayOneShot(audioSource.clip);
    }

    public void DoorSound(AudioSource audioSource)
    {
        audioSource.clip = audioClips[9];
        audioSource.PlayOneShot(audioSource.clip);
    }
}
