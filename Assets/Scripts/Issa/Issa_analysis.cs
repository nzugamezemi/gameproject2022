using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.InputSystem;

public class Issa_analysis : MonoBehaviour
{
    public jmp.Inventory inventory;
    public jmp.analysis analysis;
    public jmp.analysisView analysisView;


    Animator analysisAni;

    void Start()
    {
        analysisAni = GetComponentInChildren<Animator>();
    }


   
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //analysisUI.SetActive(false);
        }
    }
    public void ReadyAnalyze()
    {
        analysisAni.SetBool("Visible", true);
    }
    public void InvalidAnalyze()
    {
        analysisAni.SetBool("Visible", false);
    }

}
