using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using System;

public class Issa_pause : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject Canvas;
    public GameObject pauseUIprefab;

    GameObject player;

    public Selectable defaultselect;

    public SaveManager saveManager;
    PlayersManager playersManager;

    private void OnEnable()
    {
       
    }
    private void Start()
    {
        player = GameObject.Find("Players");
        saveManager = player.GetComponent<SaveManager>();
        playersManager = player.GetComponent<PlayersManager>();

        Canvas.SetActive(true);
        pauseUIprefab.SetActive(false);
        // howtoUIprefab.SetActive(false);
       
    }
    public void Update()
    {
        //if (Gamepad.current.startButton.isPressed)
        //{
        //    pauseUIprefab.SetActive(true);
        //    playersManager.pauseUI = true;
        //    Time.timeScale = 0f;
        //}
        
    }

    bool isPaused = false;
    
    private void OnPause()
    {
        if (isPaused)
        {
            Backtopkay();

        }
        else
        {
            if (defaultselect)
            {
                defaultselect.Select();
            }
            pauseUIprefab.SetActive(true);
            playersManager.pauseUI = true;
            //Time.timeScale = 0f;
            isPaused = true;
        }
        
    }

    public void BackStartScene()
    {
        //Time.timeScale = 1.0f;
        saveManager.Save();
        SceneManager.LoadScene("title scene");

    }
   
    

  public void Backtopkay()
    {
        pauseUIprefab.SetActive(false);
        playersManager.pauseUI = false;
        //Time.timeScale = 1.0f;
        isPaused = false;
    }

   
    // Update is called once per frame
    
}
