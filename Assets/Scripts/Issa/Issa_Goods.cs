using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Imageオブジェクトにつけるやつ
public class Issa_Goods : MonoBehaviour
{

    GameObject itemObject;
    string itemName;
    Image image;


    private void Awake()
    {
        image = GetComponent<Image>();

        Debug.Log("Clear!");
    }

    public void SetUp(Issa_obtainable item)
    {

        Debug.Log("Clear");
        image = GetComponent<Image>();//Imageコンポーネント

        this.itemName = item.GetItemName();//アイテム名を取得

        // 画像を取得してImageコンポーネントに入れる
        image.sprite = Issa_itemmaneger.GetInstance().GetItem(this.itemName).GetIcon();
        this.itemObject = item.GetGameObject();//オブジェクトを取得
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
