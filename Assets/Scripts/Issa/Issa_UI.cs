using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Issa_UI : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem particle;

    // Start is called before the first frame update
    void Start()
    {
        

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Treasure")
        {
            // パーティクルシステムのインスタンスを生成する。
            ParticleSystem newparticle = Instantiate(particle);

            // パーティクルの発生場所をこのスクリプトをアタッチしているGameObjectの場所にする。
            newparticle.transform.position = this.transform.position;

            // パーティクルを発生させる。
            newparticle.Play();

            // インスタンス化したパーティクルシステムのGameObjectを削除する。(任意)
            // ※第一引数をnewParticleだけにするとコンポーネントしか削除されない。
            Destroy(newparticle.gameObject, 5.0f);


        }
    }

    // Update is called once per frame
    void Update()         
    {

    }
    

}
