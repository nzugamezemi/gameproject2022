using System.IO;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartSelect : MonoBehaviour
{
    public Selectable defaultSelect;
    public Selectable ds;
    SaveDetail save;
    public static bool saveDate = false;

    AudioSource audio;
    public AudioClip audioClip;
    public AudioClip canselaudio;

    // Start is called before the first frame update
    void Start()
    {
        if (defaultSelect)
        {
            defaultSelect.Select();


        }

        //string filePath = Application.persistentDataPath + "/savedate.json";
        string filePath = Application.dataPath + "/savedate.json";

        if (File.Exists(filePath))
        {
            string data = File.ReadAllText(filePath);
            save = JsonUtility.FromJson<SaveDetail>(data);

            saveDate = save.save;
        }

        audio = GetComponent<AudioSource>();
        
    }

    public void GameStart()
    {
        saveDate = false;
        audio.clip = audioClip;
        audio.PlayOneShot(audio.clip);
        SceneManager.LoadScene("Prologue");

    }

    public void LoadGameStart()
    {
        if (saveDate)
        {
            audio.clip = audioClip;
            audio.PlayOneShot(audio.clip);

            SceneManager.LoadScene("Main");
        }
        else
        {
            audio.clip = canselaudio;
            audio.PlayOneShot(audio.clip);
        }
        
        
        
    }

    public static bool SD()
    {
        return saveDate;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
