using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


//取得可能クラス
[Serializable]
public class Issa_obtainable
{

    [SerializeField] public string itemName;//アイテム名
    GameObject gameObject;//アイテムのオブジェクト

    internal void Obtain(GameObject item)
    {
        gameObject = item;
        Issa_inventory.GetInstance().Obtain(this);
    }

    internal string GetItemName()
    {
        return itemName;
    }

    internal GameObject GetGameObject()
    {
        return gameObject;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
