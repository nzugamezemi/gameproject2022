using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Issa_target2 : MonoBehaviour
{

    //キャンバスの情報
    [SerializeField]
    private Canvas canvas;

    //追従するオブジェクト
    [SerializeField]
    private Transform targetTransform;

    private RectTransform canvasRectTransform;

    //UIのRectTransformの情報
    private RectTransform myRectTransform;

    //キャラクターの頭上にずらすための数値
    public Vector3 offset = new Vector3(1.5f, 1.5f, 0);


    // Start is called before the first frame update
    void Start()
    {
        myRectTransform = GetComponent<RectTransform>();
        canvasRectTransform = canvas.GetComponent<RectTransform>();
        

    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector2 pos;

        //ターゲットのスクリーン座標
        Vector2 screenPos = RectTransformUtility.WorldToScreenPoint(Camera.main, targetTransform.position + offset);
        //Debug.Log(screenPos);

        //スクリーン座標からカメラでのローカル座標に変換
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRectTransform, screenPos, Camera.main, out pos);

        myRectTransform.localPosition = pos;



        //ここからは画面外で画面は端になるような処理をする
        
        //画面のスクリーン座標の中心
        var center = 0.5f * new Vector2(Screen.width, Screen.height);

        //画面外のライン決め
        float d = Mathf.Max(
            Mathf.Abs(myRectTransform.anchoredPosition.x / center.x),
            Mathf.Abs(myRectTransform.anchoredPosition.y / center.y)
        );
       
        //ターゲットが画面外に出たときの判定
        bool isOffscreen = (d > 1f);
        if (isOffscreen)
        {
            
        }
    }
}
