using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Issa_inventory : MonoBehaviour
{

    [SerializeField] Transform content;
    [SerializeField] GameObject imagePrefab;

    public List<Issa_Goods> inventory = new List<Issa_Goods>();

    
    static Issa_inventory instance;

    Issa_analysis countTaker;

    public static Issa_inventory GetInstance()
    {
        return instance;
    }


    //アイテムを取得するメソッド
    public void Obtain(Issa_obtainable item)
    {

        //アイテムの存在を確認
        if (Issa_itemmaneger.GetInstance().HasItem(item.GetItemName()))
        {


            GameObject goodsObj = Instantiate(imagePrefab, content);//Imageインスタンスを作る
            Issa_Goods goods = goodsObj.GetComponent<Issa_Goods>();//scriptを取得
            goods.SetUp(item);
            item.GetGameObject().SetActive(false);//アイテムをヒアクティブにする
            inventory.Add(goods);//リストに入れる

            Debug.Log("アイテムを取得");





        }
        else
        {
            
        }
    }

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        countTaker = new Issa_analysis();
    }
    // Update is called once per frame
    public void AnalaysResult()
    {

    }
}
