using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Issa_camera : MonoBehaviour
{
    // Start is called before the first frame update

    private GameObject player;
    private Vector3 offset;
    void Start()
    {
        //プレイヤーに情報を格納
        this.player = GameObject.Find("Cube");
        //カメラとプレイヤーの相対距離
        offset = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = this.player.transform.position + offset;
    }
}
