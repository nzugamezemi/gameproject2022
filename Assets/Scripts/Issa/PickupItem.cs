using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
class ItemInfo
{
    [SerializeField] public string type;
    [SerializeField] public string displayName;
    [SerializeField] public Image itemimage;
}
public class PickupItem : MonoBehaviour

{

     private ItemInfo info;

     public GameObject pickupItem;

    [SerializeField]
    private GameObject inventory;
    public void OnTriggerStay(Collider other)
    {

        if (other.gameObject.CompareTag("Player"))
        {
            if (Input.GetKey(KeyCode.D))
            {
                pickupItem.SetActive(false);

            }
        }
    }

}

public class InventoryItem
{
    private ItemInfo info;
}
