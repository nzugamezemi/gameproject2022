using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace jmp
{
    public class analysis : MonoBehaviour
    {

        public Action OnAnalysisChanged;

        public GameObject anaView;
        analysisView AnalysisView;

        public Dictionary<int, List<ItemInfo>> analyzeditems = new Dictionary<int, List<ItemInfo>>();
        private void OnEnable()
        {
             AnalysisView = anaView.gameObject.GetComponentInChildren<analysisView>();
            anaView.SetActive(false);
        }
        private void Start()
        {
        }
        // Start is called before the first frame update
        public void OnAnalysis()
        {
            anaView.SetActive(true);
            //ここで通知を送る
            OnAnalysisChanged?.Invoke();

        }

      
        public void BigAnalysis(ItemInfo info)
        {

            AnalysisView.BigAnalysis(info);

            anaView.SetActive(true);
            if (!analyzeditems.ContainsKey(info.itemNum))
            {
                analyzeditems[info.itemNum] = new List<ItemInfo>();
            }
            analyzeditems[info.itemNum].Add(info);

            Debug.Log(info.itemNum +":"+analyzeditems.ContainsKey(info.itemNum));


        }

        public void analysisAdd(ItemInfo info)
        {
            if (!analyzeditems.ContainsKey(info.itemNum))
            {
                analyzeditems[info.itemNum] = new List<ItemInfo>();
            }
            analyzeditems[info.itemNum].Add(info);

            Debug.Log(analyzeditems[info.itemNum]);
        }

        public bool Has(int itemNum)
        {

            if (analyzeditems.ContainsKey(itemNum))
            {

                return true;
            }
            else
            {
                return false;
            }
        }

        // Update is called once per frame
      
    }
}
