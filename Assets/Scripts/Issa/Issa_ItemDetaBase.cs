using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName ="ItemDataBase", menuName ="CreateItemDataBase")]

public class Issa_ItemDetaBase : ScriptableObject
{

    [SerializeField]
    private List<Issa_Item> itemLists = new List<Issa_Item>();

    //アイテムリストを返す
    public List<Issa_Item> GetItemLists()
    {
        return itemLists;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
