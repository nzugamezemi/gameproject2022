using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Issa_itemmaneger : MonoBehaviour
{

    //アイテムデータベース
    [SerializeField]
    private Issa_ItemDetaBase itemDetaBase;
    //アイテム数管理
    private Dictionary<Issa_Item, int> numOfItem = new Dictionary<Issa_Item, int>();


    static Issa_itemmaneger instance;

    public static Issa_itemmaneger GetInstance()
    {
        return instance;
    }

    // Start is called before the first frame update
    void Start()
    {

        instance = this;

    }

    public bool HasItem(string searchName)
    {
        return itemDetaBase.GetItemLists().Exists(item => item.GetItemName() == searchName);
    }


    //アイテムがデータベースに存在するか調べるメソッド
    public Issa_Item GetItem(string searchName)
    {
        return itemDetaBase.GetItemLists().Find(itemName => itemName.GetItemName() == searchName);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
