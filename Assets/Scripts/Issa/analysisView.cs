using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace jmp
{
    public class analysisView : MonoBehaviour
    {
        public jmp.Inventory inventory;
        public jmp.analysis analysis;
        public GameObject anaimagePrefab;
        ItemUIDemo set;
        // Start is called before the first frame update
        void Start()
        {

        }


        private void OnEnable()
        {
            if (analysis)
            {
                analysis.OnAnalysisChanged += Analysis;

                Analysis();
            }
        }
        private void OnDisable()
        {
            if (analysis)
            {
                analysis.OnAnalysisChanged -= Analysis;
            }
        }
        // Update is called once per frame
        public void Analysis()
        {
            foreach (Transform child in transform)
            {
                Destroy(child.gameObject);
            }
            List<ItemInfo> anItems = inventory.items
                     .SelectMany(x => x.Value) // ItemInfoだけを取り出す
                     .OrderBy(x => x.pickupTime) // 取得時間で並び変える
                     .ToList(); // Listに変換

            foreach(ItemInfo anInfo in anItems)
            {
                GameObject anview = Instantiate(anaimagePrefab);
                anview.transform.SetParent(transform, false);

                ItemUIDemo set = GetComponent<ItemUIDemo>();
                jmp.analysisItem analysisItem = anview.GetComponent<jmp.analysisItem>();
                if (analysisItem)
                {
                    set.set();
                    analysisItem.SetanaInfo(anInfo);
                }
                analysis.analysisAdd(anInfo);
            }



        }
        public void BigAnalysis(ItemInfo info)
        {
            GameObject anview = Instantiate(anaimagePrefab);
            anview.transform.SetParent(transform, false);

            ItemUIDemo set = GetComponent<ItemUIDemo>();
            jmp.analysisItem analysisItem = anview.GetComponent<jmp.analysisItem>();
            if (analysisItem)
            {
                analysisItem.SetanaInfo(info);
                set.set();

            }



        }

    }
}
