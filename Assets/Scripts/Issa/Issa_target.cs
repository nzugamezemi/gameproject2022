using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Issa_target : MonoBehaviour

{


    [SerializeField]
    private RectTransform canvasRecttransform;

    public Transform target = default;

    private Camera mainCamera;

    private RectTransform rectTransform;

    public GameObject players;

    private Vector3 offset = new Vector3(0, 1.5f , 0);


    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;

        rectTransform = GetComponent<RectTransform>();

        PlayersManager robTransform = players.GetComponent<PlayersManager>();

        target = robTransform.robotPrefab.transform;

    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector2 pos_sc;

        Vector2 screenPos = RectTransformUtility.WorldToScreenPoint(Camera.main, rectTransform.position + offset);

        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRecttransform, screenPos, Camera.main, out pos_sc);

        rectTransform.position = pos_sc;

        float canvasScale = transform.localScale.z;

        var center = 0.5f * new Vector3(Screen.width, Screen.height);//画面中央の座標

        // （画面中心を原点(0,0)とした）ターゲットのスクリーン座標を求める
        var pos = mainCamera.WorldToScreenPoint(target.position) - center;

        if (pos.z < 0f)
        {
            pos.x = -pos.x;
            pos.y = -pos.y;
        }

        if(Mathf.Approximately(pos.y, 0f)){
            pos.y = -center.y;
        }

        var halfSize = 0.5f * rectTransform.sizeDelta * canvasScale;

        float d = Mathf.Max(
            Mathf.Abs(pos.x /  (center.x - halfSize.x)),
            Mathf.Abs(pos.y / (center.y - halfSize.y))
            );

        //ターゲットのスクリーン座標が画面外なら、画面端になるよう調整

        bool isOffscreen = (pos.z < 0f || d > 1f);

        if (isOffscreen)
        {
            pos.x /= d;
            pos.y /= d;

        }

        rectTransform.anchoredPosition = pos / canvasScale;
        

    }

    
    
}

