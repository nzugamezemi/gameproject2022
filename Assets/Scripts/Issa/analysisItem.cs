using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using System;
namespace jmp
{
    public class analysisItem : Selectable
    {
    //    public TextMeshProUGUI itemName;
    //    public TextMeshProUGUI infomation;

        public Image anaicon;
        public string displayname;
        public string iteminfomation;

        public jmp.ItemInfo anaiteminfo;

        public Action<jmp.analysisItem> OnSelection;

        public void SetanaInfo(jmp.ItemInfo info)
        {
            anaiteminfo = info;


            displayname = info.displayName;
            iteminfomation = info.itemtext1 + "\n" + info.itemtext2 + "\n" + info.itemtext3 + "\n" +info.itemtext4 + "\n" + info.itemtext5;
            //itemName.text = info.displayName;
            //infomation.text = info.itemtext;
            anaicon.sprite = info.ana_UI;
        }
        public override void OnSelect(BaseEventData eventData)
        {
            // 通常の動作
            base.OnSelect(eventData);

            // 終わったら通知を送信する
            OnSelection?.Invoke(this);

        }
    }
}
