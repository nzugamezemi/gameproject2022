using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    Dictionary<string, List<ItemInfo>> items = new Dictionary<string, List<ItemInfo>>();
    // Start is called before the first frame update
   void GetItem(ItemInfo info)
    {
        // タイプに合った配列がなければ作っておく
        if (!items.ContainsKey(info.type))
        {
            items[info.type] = new List<ItemInfo>();
        }
        items[info.type].Add(info);
    }

    public int Count(string itemType)//アイテム数
    {
        if (items.ContainsKey(itemType))
        {
            return items[itemType].Count;
        }
        else
        {
            return 0;
        }
    }

    public bool Has(string itemType)//アイテム所持の判定
    {
        return Count(itemType) > 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
