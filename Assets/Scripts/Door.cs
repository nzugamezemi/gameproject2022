using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    //public GameObject key;

    public Vector3 move;
    public Quaternion rotate = Quaternion.Euler(0, 0, 0);
    public float speed = 2f;
    public float moveTime = 5f;
    public bool On = false;

    Gimmicks gimmicks;
    Collider boxCollider;

    GameObject openDoor;

    Transform openDoorTransform;
    Collider openDoorBoxCollider;

    Transform door;
    float time;

    SoundManager soundManager;
    AudioSource audioSource;

    bool sound;

    void Start()
    {
        door = this.transform;
        openDoor = transform.GetChild(1).gameObject;
        openDoorTransform = openDoor.transform;
        openDoorBoxCollider = openDoor.GetComponent<Collider>();

        time = moveTime;
        gimmicks = GetComponentInChildren<Gimmicks>();
        boxCollider = GetComponent<BoxCollider>();

        soundManager = GameObject.Find("Players").GetComponent<SoundManager>();
        audioSource = GetComponent<AudioSource>();

        sound = false;
        // key.transform.parent = transform.parent;
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log(other);

        if (other.gameObject.layer == LayerMask.NameToLayer("Key") && other.gameObject.tag == "Pickup")
        {
            On = true;
        }
    }

    void ToOpen()
    {
        if (On == true)
        {
            boxCollider.enabled = false;
            time -= Time.deltaTime;
            //door.position += move * speed * Time.deltaTime;
            //door.rotation = door.rotation * rotate;
            
            if (!sound)
            {
                soundManager.DoorSound(audioSource);
                sound = true;
            }
            
            openDoorBoxCollider.enabled = false;
            openDoorTransform.rotation = openDoorTransform.rotation * rotate;

            if (time <= 0)
            {
                On = false;
                gimmicks.excution = true;
               // openDoor.SetActive(false);
            }
        }
    }

    void FixedUpdate()
    {
        ToOpen();
    }
}
