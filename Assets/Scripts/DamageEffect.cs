using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageEffect : MonoBehaviour
{
    public Transform target;
    public float cameraDistance = 10;


    // Start is called before the first frame update
    void Start()
    {
        target = transform.parent;

        transform.parent = Camera.main.transform;
        transform.localPosition = new Vector3(0, 0, cameraDistance);
    }

}
