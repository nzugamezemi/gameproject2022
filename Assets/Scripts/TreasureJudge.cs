using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System;

public class TreasureJudge : MonoBehaviour
{
    public TreasureDetail treasureDetail;
    public bool treasureAreaEnter = false;

    public jmp.Inventory inventory;
    public jmp.ItemInfo itemInfo = new jmp.ItemInfo();

    public List<GameObject> inventoryObj;

    public bool isBring = false;

    public bool itemPikking;

    public Rigidbody pickupTarget;
    public Joint joint;

    [SerializeField]int maxTreasure = 5;
    int treasureCount;

    public GameObject enterObject;

    TreasureType treasureType;
    public Player player;
    PlayersManager playersManager;
    public Issa_ani itemAni;
    CapsuleCollider treCol;

    SoundManager soundManager;
    AudioSource audioSource;

    PlayerActionSender actionSender;

    private void OnEnable()
    {
        inventory = GameObject.Find("TestInventory")?.GetComponent<jmp.Inventory>();
        player = transform.root.GetComponent<Player>();
    }

    private void Start()
    {
        
        playersManager = GameObject.Find("Players").GetComponent<PlayersManager>();
        audioSource = GetComponent<AudioSource>();
        actionSender = GetComponent<PlayerActionSender>();

        if (playersManager)
        {
            soundManager = playersManager.soundManager;
        }

        if(actionSender != null)
        {
            actionSender.listeners = playersManager.playerActionListeners;
        }
    }

    // 宝が入手できる範囲の判定
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Treasure")
        {
            treasureAreaEnter = true;
            enterObject = other.gameObject;
            treasureDetail = other.gameObject.GetComponent<TreasureDetail>();
            treasureType = treasureDetail.type;
            itemInfo = treasureDetail.Infomation;
            itemAni = other.gameObject.GetComponentInChildren<Issa_ani>();



            if (treasureType == TreasureType.BigTreasure)
            {
                Debug.Log("Pickup: " + other.name);
                pickupTarget = other.attachedRigidbody;
                treCol = other.gameObject.transform.GetChild(0).GetComponent<CapsuleCollider>();
            }
        }
       /* else
        {
            itemAni = null;
        }*/

        if ((other.gameObject.layer == LayerMask.NameToLayer("Key")))
        {
            //Debug.Log("Pickup: " + other.name);
            enterObject = other.gameObject;
            pickupTarget = other.attachedRigidbody;
        }

        if (other.gameObject.name == "Robot(Clone)")
        {
            player.robotEnter = true;
            if (pickupTarget == null)
            {
                enterObject = other.gameObject;
                pickupTarget = other.attachedRigidbody;
            }
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == enterObject)
        {
            if (other.gameObject.tag == "Treasure")
            {
                if (treasureType == TreasureType.BigTreasure)
                {
                    Debug.Log("Pickup: " + other.name);

                    treasureDetail = null;
                    enterObject = null;
                    pickupTarget = null;
                    treasureAreaEnter = false;
                    itemInfo = null;
                    itemAni = null;
                }

                if (treasureType == TreasureType.SmallTreasure)
                {
                    treasureDetail = null;
                    enterObject = null;
                    treasureAreaEnter = false;
                    itemInfo = null;
                    itemAni = null;
                }
            }

            if ((other.gameObject.layer == LayerMask.NameToLayer("Key")))
            {
                enterObject = null;
                pickupTarget = null;
            }
            /*treasureDetail = null;
            enterObject = null;
            treasureAreaEnter = false;
            itemInfo = null;*/

            //itemAni = null;
            //treCol = null;

            //other.transform.GetChild(0).gameObject.SetActive(false);
        }

        if (other.gameObject.name == "Robot(Clone)")
        {
            player.robotEnter = false;
           // enterObject = null;
            pickupTarget = null;
        }
    }


    // 宝のサイズ別の動作
    public void TreasureSize(List<GameObject> treasure, bool input)
    {
        switch (treasureType)
        {
            case TreasureType.SmallTreasure:
                if (input && treasureCount != maxTreasure)
                {
                    //SmallTreasureGet();
                    //treasure.Add(enterObject);
                    IssaSmallTrasure();
                }
                break;
            case TreasureType.BigTreasure:
                OnGrab(input);
                break;
        }
    }


    void IssaSmallTrasure()
    {
        itemAni.UIClose();
        inventory.GetItem(itemInfo);
        soundManager.ItemGetSound(audioSource);
        playersManager.nullGameObjects.Add(enterObject);
        TreasureDetail treasureDetail = enterObject.GetComponent<TreasureDetail>();
        itemAni = null;
        inventoryObj.Add(enterObject);
        treasureDetail.get = true;
        //enterObject.SetActive(false);

        enterObject = null;
        treasureAreaEnter = false;
        treasureDetail = null;

        actionSender.BroadcastMessage("OnPlayerAction", "GetItem");
    }

    // 小さいサイズの処理
    void SmallTreasureGet()
    {
        if (enterObject != null)
        {
            treasureCount++;
            //obt.Obtain(gameObject); //取得メソッド
            playersManager.nullGameObjects.Add(enterObject);
            treasureDetail = null;
            treasureAreaEnter = false;
            
        }
    }


    void OnGrab(bool isGrabbing)
    {
        Debug.Log("Grab: " + isGrabbing);
        //isBring = isGrabbing;

        if (!isGrabbing && joint != null)
        {
            Destroy(joint);
            joint = null;
            if (pickupTarget != null)
            {
                Vector3 p = pickupTarget.position;
                p.z = transform.parent.position.z;
                pickupTarget.position = p;
                if (itemAni)
                {
                    itemAni.UIShown();
                    treCol.enabled = true;
                    itemPikking = false;
                }
                
               // treCol.enabled = true;
               // itemPikking = false;
                
            }
        }

        if (isGrabbing && pickupTarget != null)
        {
            joint = transform.parent.gameObject.AddComponent<FixedJoint>();
            joint.connectedBody = pickupTarget;
            Vector3 d = pickupTarget.position - joint.transform.position;
            joint.connectedAnchor = d;

            if (itemAni)
            {
                itemAni.UIClose();

                if(enterObject.tag != "Pickup")
                {
                    treCol.enabled = false;
                    itemPikking = true;
                    actionSender.BroadcastMessage("OnPlayerAction", "BigTresureGrab");
                }
            }

            // treCol.enabled = false;
            // itemPikking = true;
            // actionSender.BroadcastMessage("OnPlayerAction", "BigTresureGrab");

        }
    }

    void OnGrab(InputValue inputValue)
    {
        if (treasureAreaEnter)
        {
            TreasureSize(player.treasureList, inputValue.isPressed);

        }
        else
        {
            OnGrab( inputValue.isPressed);
        }
    }

    void FixedUpdate()
    {
        if(joint != null)
        {
            isBring = true;
        }
        else
        {
            isBring = false;
        }
    }
}
