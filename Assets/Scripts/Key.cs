using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    public TreasureJudge tj;
    //Transform key = null;

    void Start()
    {
        tj = GameObject.Find("Diver(Clone)").GetComponentInChildren<TreasureJudge>();
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log(other);

        if ((other.gameObject.layer == LayerMask.NameToLayer("Door")))
        {
            tj.pickupTarget = null;
            gameObject.SetActive(false); // gameObjectを非アクティブ化
        }
    }

    void FindDiver()
    {
        if(tj == null)
        {
            tj = GameObject.Find("Diver(Clone)").GetComponentInChildren<TreasureJudge>();
        }
    }

    void fixedUpdate()
    {
        FindDiver();
    }
}
