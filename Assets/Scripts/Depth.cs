using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Depth : MonoBehaviour
{
    float m_Y;

    public GameObject target;
    GameObject diverResPos;
    public GameObject depthMin;
    GameObject depthUI;

    Slider depthSlider;
    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player");
        diverResPos = GameObject.Find("DiverRespawnPosition");
        depthUI = GameObject.Find("Depth");
        depthMin = diverResPos.transform.GetChild(0).gameObject;

        DepthSlider();
    }

    private void Update()
    {
        if (target != null)
        {
            m_Y = target.transform.position.y;
            depthSlider.value = m_Y;
        }
    }

    void DepthSlider()
    {
        if (depthUI != null)
        {
            depthSlider = depthUI.GetComponent<Slider>();
            depthSlider.maxValue = diverResPos.transform.position.y;
            depthSlider.minValue = depthMin.transform.position.y;
        }
    }
}
