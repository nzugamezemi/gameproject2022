using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectControl : MonoBehaviour
{
    GameObject flow;
    public float activeTime = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        flow = transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (!flow.activeSelf)
        {
            StartCoroutine("FlowCoroutine");
        }
    }

    IEnumerator FlowCoroutine()
    {
        yield return new WaitForSeconds(activeTime);
        flow.SetActive(true);
    }
}
