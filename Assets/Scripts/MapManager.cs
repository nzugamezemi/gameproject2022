using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    TreasureDetail treasureDetail;

    [SerializeField] int treasureCount;

    public Transform treasurePoints;
    public GameObject[] treasurePoint;

    public GameObject Treasure;

    private void OnEnable()
    {
        treasurePointManager();
    }

    private void Start()
    {
        TreasureInstatinate();
    }

    void treasurePointManager()
    {
        treasurePoint = new GameObject[treasurePoints.childCount];
        for (var i = 0; i < treasurePoints.childCount; i++)
        {
            treasurePoint[i] = treasurePoints.transform.GetChild(i).gameObject;
        }
    }

    List<int> num = new List<int>();

    void TreasureInstatinate()
    {
        int n = 0;

        while (n < treasureCount)
        {
            int rnd = Random.Range(0, treasurePoint.Length);

            if (!num.Contains(rnd))
            {
                GameObject t = Instantiate(Treasure, treasurePoint[rnd].transform.position, treasurePoint[rnd].transform.rotation);

                treasureDetail = t.GetComponent<TreasureDetail>();
                treasureDetail.number = n;
                num.Add(rnd);
                n++;
            }
            else
            {
                continue;
            }
        }
        
    }
}
