using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeJoint : MonoBehaviour
{
    public float smoothness = 0.9f;

    Quaternion rotation;

    // Start is called before the first frame update
    void Start()
    {
        rotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Lerp(transform.parent.rotation, rotation, smoothness);
        rotation = transform.rotation;
    }
}
