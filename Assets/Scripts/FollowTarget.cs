using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    public GameObject target;
    public float attackSpeed = 5f;
    public float deceleration = 2f;
    public float minDistance = 0.5f;
    public float smoothing = 0.9f;

    float speed = 0;

    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        // Z軸の移動がない
        rb.constraints = RigidbodyConstraints.FreezePositionZ;
    }

    // 物理演算を使うので、FixedUpdateを利用
    void FixedUpdate()
    {
        if (target)
        {
            Vector3 delta = target.transform.position - transform.position;
            Vector3 direction = delta.normalized;
            float distance = delta.magnitude;

            // 減速する
            if (speed > 0)
            {
                speed -= Time.deltaTime * deceleration;
                if (speed < 0)
                {
                    speed = 0;
                }
            }

            if (distance > minDistance)
            {
                // ターゲットの方向に向く
                Vector3 up = new Vector3(delta.y, -delta.x, 0);
                if (up.y < 0)
                {
                    up *= -1f;
                }

                Quaternion targetRotation = Quaternion.LookRotation(direction, up);
                transform.rotation = Quaternion.Lerp(targetRotation, transform.rotation, smoothing);
            }

            // ターゲットへ突進する
            if (Input.GetKeyDown(KeyCode.Space))
            {
                speed = attackSpeed;
            }

            // 移動する
            if (rb)
            {
                rb.velocity = transform.forward * speed;
            }
        }
    }
}
