using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemVel : MonoBehaviour
{

    public float maxSpeedSide = 0f;
    public float maxSpeedUp = 2f;
    public float maxSpeedDown = -2f;
    // public float maxSpeedUp = 0f;

    GameObject diver;
    public string boxName;

    //TreasureJudge tj;
    //public GameObject enterObject;
    public bool isBring;
    Vector3 vel = Vector3.zero;
    Rigidbody rb;
    public Rigidbody item;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        diver = GameObject.Find("Diver(Clone)");
       // tj =  GameObject.Find("Diver(Clone)").transform.Find("Hold").GetComponent<TreasureJudge>();
        boxName = this.gameObject.name;
    }

    void VelocityChange()
    {
        vel = rb.velocity;

        if (Mathf.Abs(vel.x) > maxSpeedSide)
        {
            vel.x = maxSpeedSide * Mathf.Sign(vel.x);
        }
        if (vel.y < maxSpeedDown)
        {
            vel.y = maxSpeedDown;
        }
        if (vel.y > maxSpeedUp)
        {
            vel.y = maxSpeedUp;
        }
        rb.velocity = vel;
    }

    void FindDiver()
    {
        diver = GameObject.Find("Diver(Clone)");
    }

    void FixedUpdate()
    {
        // ���x����
        // Vector3 vel = rb.velocity;
        //enterObject = tj.enterObject;
        //item = tj.joint.connectedBody;
        if(diver == null)
        {
            FindDiver();
        }

        if (diver != null)
        {
            if(diver.GetComponent<FixedJoint>() != null)
            {
                item = diver.GetComponent<FixedJoint>().connectedBody;

                if (item != null)
                {
                    if (rb == item)
                    {

                    }
                    else
                    {
                        VelocityChange();
                    }
                }
            }
            else
            {
                item = null;
                VelocityChange();
            }
        }
    }
    
}
