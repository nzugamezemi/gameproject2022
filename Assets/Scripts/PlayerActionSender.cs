using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionSender : MonoBehaviour
{
    public GameObject[] listeners;

    void OnPlayerAction(string action)
    {
        foreach(GameObject listener in listeners)
        {
            listener.BroadcastMessage("OnPlayerAction", action, SendMessageOptions.DontRequireReceiver);
        }
    }
}
