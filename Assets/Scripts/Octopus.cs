using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Octopus : MonoBehaviour
{
    public Vector3 move;
    public Quaternion rotate = Quaternion.Euler(0, 0, 0);
    public float speed = 2f;
    public float moveTime = 5f;
    public bool On = false;
    public Animator animator;

    Transform oct;
    Collider collider;
    public GameObject octopus;
    public Player player;
    float time;

    AudioSource audioSource;
    Gimmicks gimmicks;
    SoundManager soundManager;

    void Start()
    {
        oct = octopus.transform;
        animator = GetComponent<Animator>();
        collider = GetComponent<BoxCollider>();
        player = GameObject.Find("Robot(Clone)").GetComponent<Player>();
        time = moveTime;
        gimmicks = GetComponent<Gimmicks>();
        soundManager = GameObject.Find("Players").GetComponent<SoundManager>();
        audioSource = GetComponent<AudioSource>();
    }

    //Vector3 Destination = Door.transform.position.magnitude + Move;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy" || other.gameObject.layer == LayerMask.NameToLayer("Electricity") && player.electricity)
        {
            On = true;
            collider.enabled = false;
            if (animator != null)
            {
                animator.SetTrigger("Damage"); // ダメージトリガーを実行する
            }
        }
    }

    void ToOpen()
    {
        if (On == true)
        {
            time -= Time.deltaTime;
            oct.position += move * speed * Time.deltaTime;
            oct.rotation = oct.transform.rotation * rotate;

            soundManager.OctopusSound(audioSource);
            if (time <= 0)
            {
                On = false;
                gimmicks.excution = true;
                gameObject.SetActive(false);
            }
        }
    }

    void FindPlayer()
    {
        if(player == null)
        {
            player = GameObject.Find("Robot(Clone)").GetComponent<Player>();
        }
    }

    void FixedUpdate()
    {
        ToOpen();

        FindPlayer();
    }
}
