using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageAreaControl : MonoBehaviour
{
    [SerializeField]int damageValue = 100;

    public int DV
    {
        get { return damageValue; }
        set { damageValue = value; }
    }

}
