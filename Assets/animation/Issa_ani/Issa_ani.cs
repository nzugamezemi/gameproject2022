using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class Issa_ani : MonoBehaviour
{
    public Animator animator;
    //public GameObject UIani;
    //public Vector3 offset;
    GameObject item;
    //List<GameObject> UIobj = new List<GameObject>();
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            UIShown();
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
             UIClose();

           
        }
    }
    private void OnDisable()
    {
        
    }
    private void Start()
    {

    }

    public void UIShown()//UIの表示の関数
    {

        animator.SetBool("Show", true);
        animator.SetBool("IsShown", true);
        animator.SetBool("Hide", false);
    }

    public void UIClose()
    {
        animator.SetBool("IsShown", false);

        animator.SetBool("Hide", true);
    }
    


}
